 Address    Code        Basic                     Source

0x00400000  0x04010013  bgez $0,0x00000013    21   b	ser # branch always to ser; is assembled to: bgez $0, ser
0x00400004  0x34070001  ori $7,$0,0x00000001  26   xn:	ori $7, $0, 1    # res
0x00400008  0x0005082a  slt $1,$0,$5          27   nxt:	ble $5, $0, fexp # if [R5]<=[$0] branch; assembled to slt $1, $0, %5 and beq $1, $0, fexp
0x0040000c  0x10200005  beq $1,$0,0x00000005       
0x00400010  0x00e60018  mult $7,$6            28   	mult $7, $6      # LO=res * x
0x00400014  0x00003812  mflo $7               29   	mflo $7          # res=res * x
0x00400018  0x200f0001  addi $15,$0,0x0000000130   	addi $15, $0, 1
0x0040001c  0x00af2822  sub $5,$5,$15         31   	sub $5, $5, $15  # n=n-1
0x00400020  0x0401fff9  bgez $0,0xfffffff9    32   	b nxt
0x00400024  0x04010015  bgez $0,0x00000015    33   fexp:	b fxn
0x00400028  0x34090001  ori $9,$0,0x00000001  38   f:	ori $9, $0, 1    # res
0x0040002c  0x200f0001  addi $15,$0,0x0000000139   nfac:	addi $15, $0, 1
0x00400030  0x0008082a  slt $1,$0,$8          40   	ble $8, $0, ffac # branch if [$8]<=[$0]; assembled to slt and beq
0x00400034  0x10200005  beq $1,$0,0x00000005       
0x00400038  0x01280018  mult $9,$8            41   	mult $9, $8      # res=res*n
0x0040003c  0x00004812  mflo $9               42   	mflo $9
0x00400040  0x200f0001  addi $15,$0,0x0000000143   	addi $15, $0, 1
0x00400044  0x010f4022  sub $8,$8,$15         44   	sub $8, $8, $15  # n=n-1	
0x00400048  0x0401fff8  bgez $0,0xfffffff8    45   	b nfac           
0x0040004c  0x04010010  bgez $0,0x00000010    46   ffac:	b ff
0x00400050  0x3c011001  lui $1,0x00001001     50     lw $10, N              # number of terms; assembled in LUI and LW (this depends on location of N)
0x00400054  0x8c2a0000  lw $10,0x00000000($1)      
0x00400058  0x3c011001  lui $1,0x00001001     51     lw $13, X              # value x
0x0040005c  0x8c2d0004  lw $13,0x00000004($1)      
0x00400060  0x340b0001  ori $11,$0,0x00000001 52   	ori $11, $0, 1    # index
0x00400064  0x340c03e8  ori $12,$0,0x000003e8 53   	ori $12, $0, 1000 # approximation exp (first term always 1) (multiplied with 1000)
0x00400068  0x016a082a  slt $1,$11,$10        57   ntrm:	ble $10, $11, rdy	
0x0040006c  0x1020000d  beq $1,$0,0x0000000d       
0x00400070  0x000b2825  or $5,$0,$11          58   	or $5, $0, $11    # calculate x^n
0x00400074  0x000d3025  or $6,$0,$13          59   	or $6, $0, $13
0x00400078  0x0401ffe2  bgez $0,0xffffffe2    60   	b xn    # branch always to xn; calculate x^n
0x0040007c  0x200f03e8  addi $15,$0,0x000003e862   	addi $15, $0, 1000
0x00400080  0x00ef0018  mult $7,$15           63   	mult $7, $15      # LO=multiply with 1000
0x00400084  0x00003812  mflo $7               64   	mflo $7
0x00400088  0x000b4025  or $8,$0,$11          65   	or $8, $0, $11 
0x0040008c  0x0401ffe6  bgez $0,0xffffffe6    66   	b f               # branch always to f; calculate n!
0x00400090  0x00e9001a  div $7,$9             68   	div  $7, $9       # 1000x2^n/n! in $14
0x00400094  0x00007012  mflo $14              69   	mflo $14
0x00400098  0x018e6020  add $12,$12,$14       70   	add $12, $12, $14 # sn=s(n-1)+term
0x0040009c  0x216b0001  addi $11,$11,0x000000071   	add $11, $11, 1   # i++
0x004000a0  0x0401fff1  bgez $0,0xfffffff1    72   	b ntrm  # branch always to ntrm
0x004000a4  0x3c011001  lui $1,0x00001001     73   rdy:	  sw $12,EX # e^x in EX
0x004000a8  0xac2c0008  sw $12,0x00000008($1)      
0x004000ac  0x00000000  nop                   74     nop
