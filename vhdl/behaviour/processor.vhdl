library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.processor_types.all;
use work.memory_config.all;
use work.logger.all;

entity processor is
  port (d_busout : out bit32;
        d_busin  : in  bit32;
        a_bus    : out bit32;
        write    : out std_ulogic;
        read     : out std_ulogic;
        ready    : in  std_ulogic;
        reset    : in  std_ulogic;
        clk      : in  std_ulogic);
end processor;
