architecture behaviour of processor is
begin
  process
    variable registers : registerMap;

    variable pc : natural;

    variable curri : bit32;
    variable tmp   : bit32;
    variable hilo  : bit64;

    variable alu_s : integer;
    variable alu_t : integer;
    variable alu_d : integer;
    variable alu_i : integer;
    variable lo    : integer;
    variable hi    : integer;

    alias op  : bit6 is curri(31 downto 26);
    alias rs  : bit5 is curri(25 downto 21);
    alias rt  : bit5 is curri(20 downto 16);
    alias rd  : bit5 is curri(15 downto 11);
    alias fc  : bit6 is curri(5 downto 0);
    alias imm : bit16 is curri(15 downto 0);

    constant dontcare : bit32 := (others => '-');

    -- Debug functions/procedures
    procedure debug_hilo is
    begin
      log("mult hi: " & integer'image(hi));
      log("mult lo: " & integer'image(lo));
    end debug_hilo;

    procedure debug_msg is
    begin
      case op is
        -- R-type
        when rtype =>
          case fc is
            when add    => log("ADD");
            when orr    => log("OR");
            when sbt    => log("SUB");
            when slt    => log("SLT");
            when div    => log("DIV");
            when mult   => log("MULT");
            when mfhi   => log("MFHI");
            when mflo   => log("MFLO");
            when nop    => log("NOP");
            when others => log("UNDEF");
          end case;
          -- I-type
        when addi   => log("ADDI");
        when ori    => log("ORI");
        when lui    => log("LUI");
        when lw     => log("LW");
        when sw     => log("SW");
        when beq    => log("BEQ");
        when bgez   => log("BGEZ");
        when others => log("UNDEF");
      end case;

      log("rs:     " & integer'image(to_integer(unsigned(rs))));
      log("Alu s:  " & integer'image(alu_s));
      log("rt:     " & integer'image(to_integer(unsigned(rt))));
      log("Alu t:  " & integer'image(alu_t));
      log("rd:     " & integer'image(to_integer(unsigned(rd))));
      log("Func:   " & integer'image(to_integer(unsigned(fc))));
      log("Alu i:  " & integer'image(alu_i));
    end debug_msg;

    -- Functions / procedures for implementation

    -- Or between integers
    function int_or(a, b : in integer) return integer is
      variable ba, bb : bit32;
    begin
      ba := bit32(to_signed(a, bit32'length));
      bb := bit32(to_signed(b, bit32'length));

      return to_integer(signed(std_ulogic_vector(ba) or std_ulogic_vector(bb)));
    end int_or;

    -- Read a register
    procedure reg_read(addr : in bit5; result : out integer) is
    begin
      result := registers(to_integer(unsigned(addr)));
    end reg_read;

    -- Write a register
    procedure reg_write(addr : in bit5; data : in integer) is
    begin
      registers(to_integer(unsigned(addr))) := data;

      -- Debug info
      log("Register " & integer'image(to_integer(unsigned(addr))) & " set to " & integer'image(data));
    end reg_write;

    -- Read from emory
    procedure memory_read(addr   : in  natural;
                          result : out bit32) is
    begin
      -- Put address on bus and wait for clock
      a_bus <= std_ulogic_vector(to_unsigned(addr, 32));
      wait until clk = '1';
      if reset = '0' then
        return;
      end if;

      -- Wait until memory is idle
      loop
        if reset = '0' then
          return;
        end if;
        exit when ready = '0';
        wait until clk = '1';
      end loop;

      -- Set read and wait for clock
      read <= '1';
      wait until clk = '1';
      if reset = '0' then
        return;
      end if;

      -- Wait until memory is ready and read from memory
      loop
        wait until clk = '1';
        if reset = '0' then
          return;
        end if;

        if ready = '1' then
          result := d_busin;
          exit;
        end if;
      end loop;

      -- Wait for clock and stop read
      wait until clk = '1';
      if reset = '0' then
        return;
      end if;

      read  <= '0';
      a_bus <= dontcare;
    end memory_read;

    -- Write to memory
    procedure memory_write(addr : in natural;
                           data : in bit32) is
    begin
      -- Show write
      log("Memory address " & integer'image(addr) & " set to " & integer'image(to_integer(signed(data))));

      -- Put address on bus and wait for clock
      a_bus <= std_ulogic_vector(to_unsigned(addr, bit32'length));
      wait until clk = '1';
      if reset = '0' then
        return;
      end if;

      -- Wait until memory is idle
      loop
        if reset = '0' then
          return;
        end if;
        exit when ready = '0';
        wait until clk = '1';
      end loop;

      -- Put data on bus and wait for clock
      d_busout <= data;
      wait until clk = '1';
      if reset = '0' then
        return;
      end if;

      -- Set write
      write <= '1';

      -- Wait until memory has written data
      loop
        wait until clk = '1';
        if reset = '0' then
          return;
        end if;
        exit when ready = '1';
      end loop;

      -- Wait for clock and stop write
      wait until clk = '1';
      if reset = '0' then
        return;
      end if;

      write    <= '0';
      d_busout <= dontcare;
      a_bus    <= dontcare;
    end memory_write;

    -- Start of process
  begin
    -- Reset everything on reset
    if reset = '0' then
      log("Reset processor_behaviour");

      read     <= '0';
      write    <= '0';
      pc       := text_base_address;
      d_busout <= dontcare;

      registers := (others => 0);

      loop
        wait until clk = '1';
        exit when reset = '0';
      end loop;
    end if;

    -- Debug program counter
    log("Read, pc: " & natural'image(pc));
    memory_read(pc, curri);

    -- Regular operation
    if reset = '1' then
      pc := pc + 4;

      reg_read(rs, alu_s);
      reg_read(rt, alu_t);

      alu_i := to_integer(resize(signed(imm), bit32'length));

      -- Log the operation
      debug_msg;

      -- Case statements for operation
      case op is
        -- R-type
        when rtype =>
          case fc is
            when add => reg_write(rd, alu_s + alu_t);
            when orr => reg_write(rd, int_or(alu_s, alu_t));
            when sbt => reg_write(rd, alu_s - alu_t);
            when slt => if alu_s < alu_t then
                          reg_write(rd, 1);
                        else
                          reg_write(rd, 0);
                        end if;
            when div => lo := alu_s / alu_t;
                        hi := alu_s mod alu_t;
                        debug_hilo;
            when mult => hilo := bit64(to_signed(alu_s * alu_t, bit64'length));
                         hi := to_integer(signed(hilo(63 downto 32)));
                         lo := to_integer(signed(hilo(31 downto 0)));
                         debug_hilo;
            when mfhi   => reg_write(rd, hi);
            when mflo   => reg_write(rd, lo);
            when nop    => assert false report "Finished calculation" severity failure;
            when others => assert false report "Invalid function" severity error;
          end case;
          -- I-type
        when addi => reg_write(rt, alu_s + alu_i);
        when ori  => reg_write(rt, int_or(alu_s, alu_i));
        when lui  => reg_write(rt, to_integer(signed(imm)) * 2**16);
        when lw   => memory_read(alu_s + alu_i, tmp);
                     reg_write(rt, to_integer(signed(tmp)));
        when sw  => memory_write(alu_s + alu_i, bit32(to_unsigned(alu_t, bit32'length)));
        when beq => if alu_s = alu_t then
                      pc := pc + (alu_i * 4);
                    end if;
        when bgez => if alu_s >= 0 then
                       pc := pc + (alu_i * 4);
                     end if;
        when others => assert false report "Invalid opcode: "
                         & integer'image(to_integer(unsigned(op))) severity error;
      end case;
      log("-------------");
    end if;
  end process;
end behaviour;
