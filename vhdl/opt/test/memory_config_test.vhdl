-- File memory_config_test.vhd
-- (too) simple test bench of the conversion functions that are declared
-- in package memory_config.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.memory_config.all;
entity memory_config_test is
  port(a4    : in    std_logic_vector(3 downto 0)  := (others => '0');
       a5    : in    std_logic_vector(4 downto 0)  := (others => '0');
       a32   : in    std_logic_vector(31 downto 0) := (others => '0');
       aio4  : inout string(1 downto 1);
       aio5  : inout string(2 downto 1);
       aio32 : inout string(8 downto 1);
       ao4   : out   std_logic_vector(3 downto 0);
       ao8   : out   std_logic_vector(7 downto 0);
       ao32  : out   std_logic_vector(31 downto 0)

       );
end memory_config_test;

architecture bhv of memory_config_test is
begin
  aio4  <= binvec2hex(a4);
  aio5  <= binvec2hex(a5);
  aio32 <= binvec2hex(a32);
  ao4   <= hexvec2bin(aio4);
  ao8   <= hexvec2bin(aio5);
  ao32  <= hexvec2bin(aio32);

end bhv;
