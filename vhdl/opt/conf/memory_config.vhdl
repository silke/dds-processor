-- File memory_config.vhd
-- This package contains the constants and functions used for the
-- simple memory module used for a subset MIPS processor.
-- The locations (start addresses) of DATA and TEXT segments and the SIZE of these segments are declared.
-- The conversion functions can convert hex string to binary string (and reverse).

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
package memory_config is
  -- USER can change memory location of data and text segments and the size of these segments.
  constant text_base_address : natural := 16#400000#;
  constant text_base_size    : natural := 200;  -- each word is 4 bytes
  constant data_base_address : natural := 16#10010000#;
  constant data_base_size    : natural := 300;
  -- *******************************

  -- convert hex to binary
  function hex2bin (hex     : character) return std_logic_vector;
  -- convert a string of hex digits to binary
  function hexvec2bin (hexv : string) return std_logic_vector;
  -- convert 4 bits binary to character
  function bin2hex (bin     : std_logic_vector(3 downto 0)) return character;
  -- convert binary pattern to hex string (input is zero extended on left to multiple of 4)
  function binvec2hex (bin  : std_logic_vector) return string;

end memory_config;

package body memory_config is
  function hex2bin (hex : character) return std_logic_vector is
    variable result : std_logic_vector (3 downto 0);
  begin
    case hex is
      when '0'     => result := "0000";
      when '1'     => result := "0001";
      when '2'     => result := "0010";
      when '3'     => result := "0011";
      when '4'     => result := "0100";
      when '5'     => result := "0101";
      when '6'     => result := "0110";
      when '7'     => result := "0111";
      when '8'     => result := "1000";
      when '9'     => result := "1001";
      when 'A'|'a' => result := "1010";
      when 'B'|'b' => result := "1011";
      when 'C'|'c' => result := "1100";
      when 'D'|'d' => result := "1101";
      when 'E'|'e' => result := "1110";
      when 'F'|'f' => result := "1111";
      when 'X'|'x' => result := "XXXX";
      when others  => null;
    end case;
    return result;
  end;

  function hexvec2bin (hexv : string) return std_logic_vector is
    constant hv : string(hexv'length downto 1) := hexv;  -- range xx downto 1, to prevent NULL slice error (now NULL slice is "0 DOWNTO 1")
  begin
    if hexv'length /= 0 then
      return hex2bin(hv(hv'left)) & hexvec2bin (hv(hv'length-1 downto 1));
    else
      return "";
    end if;
  end hexvec2bin;

  function bin2hex (bin : std_logic_vector(3 downto 0)) return character is
    type     lut_tp is array (0 to 15) of character;
    constant lut : lut_tp := ('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F');
  begin
    return lut(to_integer(unsigned(bin)));
  end bin2hex;

  -- local function.
  -- If size of 'a' is not a multiple of 4, it is zero extended on the left to a multiple of 4 bits
  function multiple_of_4 (a : std_logic_vector) return std_logic_vector is
  begin
    if (a'length rem 4) /= 0 then
      return (3 downto (a'length rem 4) => '0') & a;
    else
      return a;
    end if;
  end multiple_of_4;

  -- local function: function binary vector to hex vector. Input length must be multiple of 4.
  function binvec2hex_multiple4 (bin : std_logic_vector) return string is
    constant binv : std_logic_vector(0 to bin'length-1) := bin;  -- align index
  begin
    if binv'length = 0 then
      return "";
    elsif binv'length = 4 then
      return bin2hex(binv(0 to 3)) & "";  -- &"" used to convert character to string
    else
      return bin2hex(binv(0 to 3)) & binvec2hex_multiple4(binv(4 to binv'length-1));
    end if;
  end binvec2hex_multiple4;

  function binvec2hex (bin : std_logic_vector) return string is
  begin
    return binvec2hex_multiple4(multiple_of_4(bin));
  end binvec2hex;

end memory_config;
