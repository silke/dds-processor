library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.memory_config.all;
entity simple_test_memory is
end simple_test_memory;

architecture tb of simple_test_memory is
  component memory is
    port(d_busout : out std_logic_vector(31 downto 0);
         d_busin  : in  std_logic_vector(31 downto 0);
         a_bus    : in  std_logic_vector(31 downto 0);
         clk      : in  std_ulogic;
         write    : in  std_ulogic;
         read     : in  std_ulogic;
         ready    : out std_ulogic
         );
  end component memory;

  signal d_busout : std_logic_vector(31 downto 0);
  signal d_busin  : std_logic_vector(31 downto 0) := (others => '0');
  signal a_bus    : std_logic_vector(31 downto 0) := (others => '0');
  signal clk      : std_ulogic                    := '0';
  signal write    : std_ulogic                    := '0';
  signal read     : std_ulogic                    := '0';
  signal ready    : std_ulogic;
  signal finished : boolean                       := false;

  procedure rd (addr           :     natural;
                variable data  : out std_logic_vector(31 downto 0);
                signal   d_in  : in  std_logic_vector(31 downto 0);
                signal   a_bus : out std_logic_vector(31 downto 0);
                signal   read  : out std_ulogic;
                signal   ready : in  std_ulogic) is
  begin
    wait until clk = '0';
    a_bus <= std_logic_vector(to_unsigned(addr, 32));
    read  <= '1';
    loop
      wait until clk = '0';
      exit when ready = '1';
    end loop;
    data := d_in;
    read <= '0';
    loop
      wait until clk = '0';
      exit when ready = '0';
    end loop;
  end rd;

  procedure wr (addr         : in  natural;
                data         : in  integer;
                signal d_out : out std_logic_vector(31 downto 0);
                signal a_bus : out std_logic_vector(31 downto 0);
                signal write : out std_ulogic;
                signal ready : in  std_ulogic) is
  begin
    wait until clk = '0';
    a_bus <= std_logic_vector(to_unsigned(addr, 32));
    write <= '1';
    d_out <= std_logic_vector(to_signed(data, 32));
    loop
      wait until clk = '0';
      exit when ready = '1';
    end loop;
    write <= '0';
    loop
      wait until clk = '0';
      exit when ready = '0';
    end loop;
  end wr;


begin
  mem : memory port map (d_busout, d_busin, a_bus, clk, write, read, ready);

  clk <= not clk after 10 ns when not finished;



  process
    variable do : std_logic_vector(31 downto 0);
  begin
    wait until clk = '0';
    -- read first instruction from the program
    rd(text_base_address, do, d_busout, a_bus, read, ready);
    -- read second instruction from the program
    rd(text_base_address+4, do, d_busout, a_bus, read, ready);
    -- Write data to address data_base_address+8
    wr(data_base_address+8, 20, d_busin, a_bus, write, ready);
    finished <= true;
  end process;



end tb;
