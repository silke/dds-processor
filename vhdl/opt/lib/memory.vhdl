-- File memory.vhd
-- A memory model with properties:
-- a) seperate data and text segments
--   A MIPS assembler generates the code for TEXT and DATA segment.
--   VARIABLE prg:text_segment:= <program>
--   VARIABLE data:data_segment:= <data?
--   Check that the text_base_address and data_base_size are correct (package memory_config)
-- b) a handshake protocol is used.
--
-- read from memory
--
--         data read from memory valid
--              <-------->
--
--       +---------------+
--    ---+               +------------- read; note: a_bus must be valid when read is '1'.
--             +----------------+
--    ---------+                +------ ready
--
-- write to memory
--
--  data to be written must be valid
--        <-------------->
--
--       +---------------+
--    ---+               +------------- write; note: a_bus must be valid when write is '1'.
--             +----------------+
--    ---------+                +------ ready
--
-- c) if during a read/write the address is not in the data or text segment a violation is reported
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.memory_config.all;
entity memory is
  port(d_busout : out std_logic_vector(31 downto 0);
       d_busin  : in  std_logic_vector(31 downto 0);
       a_bus    : in  std_logic_vector(31 downto 0);
       clk      : in  std_ulogic;
       write    : in  std_ulogic;
       read     : in  std_ulogic;
       ready    : out std_ulogic
       );
end memory;

architecture behaviour of memory is
  alias word_address : std_logic_vector(31 downto 2) is a_bus(31 downto 2);
  signal   d_busouti : std_logic_vector(31 downto 0);
  constant unknown   : std_logic_vector(31 downto 0) := (others => 'X');
  type     states is (idle, rd_wr_nrdy, rd_wr_rdy);
  signal   state     : states                        := idle;  -- models state of handshake protocol
begin

  process
    type text_segment is array
      (natural range text_base_address/4 to text_base_address/4+text_base_size)  -- in model has each memory location 4 bytes, therefore divide by 4
      of string(8 downto 1);
    type data_segment is array
      (natural range data_base_address/4 to data_base_address/4+data_base_size)
      of string(8 downto 1);

    variable prg : text_segment :=
      (
        --  Code  ,  --  Basic                      Source
        --        ,  --
        "04010013",  --  bgez $0,19            21   b    ser # branch always to ser; is assembled to: bgez $0, ser
        "34070001",  --  ori $7,$0,1           26   xn:  ori $7, $0, 1    # res
        "0005082a",  --  slt $1,$0,$5          27   nxt: ble $5, $0, fexp # if [R5]<=[$0] branch; assembled to slt $1, $0, %5 and beq $1, $0, fexp
        "10200005",  --  beq $1,$0,5
        "00e60018",  --  mult $7,$6            28        mult $7, $6      # LO=res * x
        "00003812",  --  mflo $7               29        mflo $7          # res=res * x
        "200f0001",  --  addi $15,$0,1         30        addi $15, $0, 1
        "00af2822",  --  sub $5,$5,$15         31        sub $5, $5, $15  # n=n-1
        "0401fff9",  --  bgez $0,-7            32        b nxt
        "04010015",  --  bgez $0,21            33   fexp:        b fxn
        "34090001",  --  ori $9,$0,1           38   f:   ori $9, $0, 1    # res
        "200f0001",  --  addi $15,$0,1         39   nfac:        addi $15, $0, 1
        "0008082a",  --  slt $1,$0,$8          40        ble $8, $0, ffac # branch if [$8]<=[$0]; assembled to slt and beq
        "10200005",  --  beq $1,$0,5
        "01280018",  --  mult $9,$8            41        mult $9, $8      # res=res*n
        "00004812",  --  mflo $9               42        mflo $9
        "200f0001",  --  addi $15,$0,1         43        addi $15, $0, 1
        "010f4022",  --  sub $8,$8,$15         44        sub $8, $8, $15  # n=n-1
        "0401fff8",  --  bgez $0,-8            45        b nfac
        "04010010",  --  bgez $0,16            46   ffac:        b ff
        "3c011001",  --  lui $1,4097           50     lw $10, N              # number of terms; assembled in LUI and LW (this depends on location of N)
        "8c2a0000",  --  lw $10,0($1)
        "3c011001",  --  lui $1,4097           51     lw $13, X              # value x
        "8c2d0004",  --  lw $13,4($1)
        "340b0001",  --  ori $11,$0,1          52        ori $11, $0, 1    # index
        "340c03e8",  --  ori $12,$0,1000       53        ori $12, $0, 1000 # approximation exp (first term always 1) (multiplied with 1000)
        "016a082a",  --  slt $1,$11,$10        57   ntrm:        ble $10, $11, rdy
        "1020000d",  --  beq $1,$0,13
        "000b2825",  --  or $5,$0,$11          58        or $5, $0, $11    # calculate x^n
        "000d3025",  --  or $6,$0,$13          59        or $6, $0, $13
        "0401ffe2",  --  bgez $0,-30           60        b xn    # branch always to xn; calculate x^n
        "200f03e8",  --  addi $15,$0,1000      62        addi $15, $0, 1000
        "00ef0018",  --  mult $7,$15           63        mult $7, $15      # LO=multiply with 1000
        "00003812",  --  mflo $7               64        mflo $7
        "000b4025",  --  or $8,$0,$11          65        or $8, $0, $11
        "0401ffe6",  --  bgez $0,-26           66        b f               # branch always to f; calculate n!
        "00e9001a",  --  div $7,$9             68        div  $7, $9       # 1000x2^n/n! in $14
        "00007012",  --  mflo $14              69        mflo $14
        "018e6020",  --  add $12,$12,$14       70        add $12, $12, $14 # sn=s(n-1)+term
        "216b0001",  --  addi $11,$11,1        71        add $11, $11, 1   # i++
        "0401fff1",  --  bgez $0,-15           72        b ntrm  # branch always to ntrm
        --"00000000",  --  nop                   73   rdy: nop
        "3c011001",  --  lui $1,4097           74     sw $12,EX # e^x in EX
        "ac2c0008",  --  sw $12,8($1)
        others => "00000000"
        );

    variable data : data_segment :=
      ("00000007", "ffffffff", others => "00000000");

    variable address  : natural;
    variable data_out : std_logic_vector(31 downto 0);

  begin
    wait until rising_edge(clk);
    address                                          := to_integer(unsigned(word_address));
    -- check text segments
    if (address >= text_base_address/4) and (address <= text_base_address/4 + text_base_size) then
      d_busouti <= unknown;
      if write = '1' then
        prg(address) := binvec2hex(d_busin);
      elsif read = '1' then
        d_busouti <= hexvec2bin(prg(address));
      end if;
    elsif (address >= data_base_address/4) and (address <= data_base_address/4 + data_base_size) then
      d_busouti <= unknown;
      if write = '1' then
        data(address) := binvec2hex(d_busin);
      elsif read = '1' then
        d_busouti <= hexvec2bin(data(address));
      end if;
    elsif read = '1' or write = '1' then  -- address not in text/data segment; read/write not valid.
      report "out of memory range" severity warning;
      d_busouti <= unknown;
    end if;
  end process;

  d_busout <= d_busouti when state = rd_wr_rdy else unknown;

  -- code below is used to model handshake; variable 'dly' can also be another value than 1 (in state idle)
  handshake_protocol : process
    variable dly : natural;             -- nmb of delays models delay
  begin
    wait until clk = '1';
    case state is
      when idle       => if read = '1' or write = '1' then state  <= rd_wr_nrdy; end if; dly := 1;
      when rd_wr_nrdy => if dly > 0 then dly                      := dly-1; else state <= rd_wr_rdy; end if;
      when rd_wr_rdy  => if read = '0' and write = '0' then state <= idle; end if;
    end case;
  end process;

  ready <= '1' when state = rd_wr_rdy else '0';

  assert_things : process(read, write, a_bus, state)
  begin
    assert not (read = '1' and write = '1') report "memory: read and write are active" severity error;
    assert (a_bus(1 downto 0) = "00") or (state = idle) report "memory :  not an aligned address" severity error;
  end process;

end behaviour;
