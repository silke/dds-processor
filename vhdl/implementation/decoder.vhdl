library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.processor_types.all;
use work.memory_config.all;

entity decoder is
  port(clk        : in  std_ulogic;
       rst        : in  std_ulogic;
       -- Memory controller connections
       mc_data_r  : in  bit32;
       mc_read    : out bit1;
       mc_write   : out bit1;
       mc_ready   : in  bit1;
       -- Program counters
       pc_in      : in  int30;
       pc_out     : out int30;
       -- Register connections
       reg_read_1 : out bit5;
       reg_read_2 : out bit5;
       reg_write  : out bit5;
       reg_we     : out bit1;
       -- Mux connections
       mux_imm    : out bit1;
       mux_mem    : out bit1;
       mux_branch : out mux_branch_t;
       mux_addr   : out bit1;
       imm        : out bit16;
       -- ALU connections
       alu_op     : out op_alu_t;
       alu_enable : out bit1;
       alu_rdy    : in  bit1);
end decoder;

architecture implementation of decoder is
  -- The decoder state machine consists of 4 states:
  -- Load init signals the memory controller for the instruction read and waits for the memory instruction.
  -- Instr updates the buffered instruction and enables the alu. Also updates the output program counter.
  -- SW and LW wait are separate states to do memory interaction when the instruction requires it.
  type   state_load_t is (load_init, fetch, instr, sw_wait, lw_wait);
  -- Load instruction and LW/SW decoder
  signal state_load      : state_load_t;
  -- Signal exec to execute state machine
  signal exec            : bit1;
  -- Program counter
  signal program_counter : int30;
  signal write_loc       : int32;
  -- Buffered memory data
  signal mem_data        : bit32;
  alias mem_op           : bit6 is mem_data(31 downto 26);
  alias mem_rs           : bit5 is mem_data(25 downto 21);
  alias mem_rt           : bit5 is mem_data(20 downto 16);
  alias mem_rd           : bit5 is mem_data(15 downto 11);
  alias mem_fc           : bit6 is mem_data(5 downto 0);
  alias mem_imm          : bit16 is mem_data(15 downto 0);
  -- buffered instruction
  signal instruction     : bit32;
  alias instr_op         : bit6 is instruction(31 downto 26);
  alias instr_rs         : bit5 is instruction(25 downto 21);
  alias instr_rt         : bit5 is instruction(20 downto 16);
  alias instr_rd         : bit5 is instruction(15 downto 11);
  alias instr_fc         : bit6 is instruction(5 downto 0);
  alias instr_imm        : bit16 is instruction(15 downto 0);

  -- initial program counter
  constant pc_init : int30 := to_signed(text_base_address, int32'length)(31 downto 2);
begin

  load_instr : process(clk, rst)
  begin
    -- reset state to new
    if rst = '0' then
      state_load      <= load_init;
      instruction     <= (others => '0');
      program_counter <= pc_init;
      alu_enable      <= '0';
    elsif rising_edge(clk) then
      -- reset alu enable
      alu_enable <= '0';
      case state_load is
        -- Initialize memory read
        when load_init =>
          if mc_ready = '1' then
            -- update buffered instruction and signal ready to alu
            instruction <= mc_data_r;
            state_load  <= fetch;
          else
            state_load <= load_init;
          end if;
        when fetch =>
          alu_enable  <= '1';
          state_load <= instr; 
        -- Wait for instruction done
        when instr =>
          if alu_rdy = '1' then
            case instr_op is
              when lw     => state_load <= lw_wait;
              when sw     => state_load <= sw_wait;
              when others => 
                -- Load new program counter when this is the last stage of the cycle.
                program_counter <= pc_in;
                state_load <= load_init;
            end case;
          else
            state_load <= instr;
          end if;
        -- Store word, trigger write instruction to mem
        when sw_wait =>
          if mc_ready = '1' then
            state_load <= load_init;
            program_counter <= pc_in;
          else
            state_load <= sw_wait;
          end if;
        -- Load word from memory, data handled by muxes and reg_we triggered on mem_rdy
        when lw_wait =>
          if mc_ready = '1' then
            state_load <= load_init;
            program_counter <= pc_in;
          else
            state_load <= lw_wait;
          end if;
      end case;
    end if;
  end process;

  -- memory address control output, 
  -- Signals if the memory address should be from the alu or from the program counter
  load_instr_control : process(state_load, instruction)
  begin
    mux_addr <= '0';
    case state_load is
      when instr =>
        case instr_op is
          when lw|sw  => mux_addr <= '1';
          when others => mux_addr <= '0';
        end case;
      when sw_wait|lw_wait => mux_addr <= '1';
      when others  =>
    end case;
  end process;

  -- Send a read when load init or when alu is ready with a LW instruction
  mc_read <= '1' when state_load = load_init else
             alu_rdy when instr_op = lw and state_load = instr else
             '0';

  -- Only send a write when state is instr and we want to
  mc_write <= alu_rdy when instr_op = sw and state_load = instr else
              '0';

  -- Set the correct opcode to the alu based on the instruction.
  decode : process(instruction)
  begin
    case instr_op is
      when rtype =>
        case instr_fc is
          when add    => alu_op <= add_c;
          when orr    => alu_op <= or_c;
          when sbt    => alu_op <= sub_c;
          when slt    => alu_op <= slt_c;
          when div    => alu_op <= div_c;
          when mult   => alu_op <= mult_c;
          when mfhi   => alu_op <= mfhi_c;
          when mflo   => alu_op <= mflo_c;
          when nop    => alu_op <= add_c;
          when others => alu_op <= add_c;
        end case;
        -- I-type
      when addi   => alu_op <= add_c;
      when ori    => alu_op <= or_c;
      when lui    => alu_op <= lui_c;
      when lw     => alu_op <= add_c;
      when sw     => alu_op <= add_c;
      when beq    => alu_op <= branch_c;
      when bgez   => alu_op <= branch_c;
      when others => alu_op <= or_c;
    end case;
  end process;

  -- Select register write enable.
  -- With normal instruction we want to write when the alu is ready
  -- Mult, div and sw don't store a result in the register map
  -- lw stores when the memory is ready.
  select_reg_we : process(instruction, alu_rdy, mc_ready, state_load)
  begin
    reg_we <= alu_rdy;
    case instr_op is
      when rtype =>
        case instr_fc is
          when div|mult|nop => reg_we <= '0';
          when others       =>
        end case;
      when lw =>
        if state_load = lw_wait then
          reg_we <= mc_ready;
        end if;
      when sw|bgez|beq => reg_we <= '0';
      when others      =>
    end case;
  end process;

  -- Set branch mux
  with instr_op select mux_branch <=
    b_gez_c  when bgez,
    b_eq_c   when beq,
    b_none_c when others;

  -- Set mux to read from memory bus
  with instr_op select mux_mem <=
    '1' when lw,
    '0' when others;

  -- Set immediate mux for instructions using imm.
  with instr_op select mux_imm <=
    '1' when addi|ori|lui|lw|sw|bgez|beq,
    '0' when others;

  -- Set write destination.
  with instr_op select reg_write <=
    instr_rd when rtype,
    instr_rt when others;

  -- Always set these to rs and rt locations.
  reg_read_1 <= instr_rs;
  reg_read_2 <= instr_rt;
  imm        <= instr_imm;
  pc_out     <= program_counter;

end implementation;
