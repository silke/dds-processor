library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.processor_types.all;

entity alu is
  port(clk   : in  std_ulogic;
       rst   : in  std_ulogic;
       op    : in  op_alu_t;
       op_en : in  bit1;
       rdy   : out bit1;
       r1    : in  int32;               -- rs | MEM
       r2    : in  int32;               -- rt | imm
       w     : out int32
       );
end alu;

architecture implementation of alu is
  -- Adder
  component alu_adder
    port (subop : in op_add_t; s, t : in int32; d : out int32);
  end component;

  signal ar1, ar2           : int32;    -- Adder element 1
  signal counter            : integer range 0 to 36;
  signal input_sign         : bit1;
  signal w_add, w_or, w_lui : int32;
  signal div_abs_r2         : int32;
  signal addop              : op_add_t;
  signal hilo               : int64;
  signal w_int              : int32;
  alias hi                  : int32 is hilo(63 downto 32);
  alias lo                  : int32 is hilo(31 downto 0);
begin
  alu_add : alu_adder port map (addop, ar1, ar2, w_add);

  -- OR instruction
  w_or <= r1 or r2;

  -- LUI instruction
  w_lui <= r2(15 downto 0) & (15 downto 0 => '0');

  -- Multiply/divide sign prediction
  input_sign <= r1(31) xor r2(31);

  -- Asynchronous
  process(op, hilo, r1, r2, div_abs_r2, counter)
  begin
    -- Adder inputs
    case op is
      when mult_c =>
        -- Multiplication adds hi instead of r1
        ar1 <= hi;
        ar2 <= r2;
      when div_c =>
        -- The divider has two pre steps and two post steps:
        --   Pre: calculate absolute values
        --   Post: increment hi and lo
        -- Regular operation is adding a fixed portion of hilo to a calculated absolute value.
        if counter = 0 then
          ar1 <= zero32;
          ar2 <= r1;
        elsif counter = 1 then
          ar1 <= zero32;
          ar2 <= r2;
        elsif counter = 34 then
          ar1 <= zero32;
          ar2 <= lo;
        elsif counter = 35 then
          ar1 <= zero32;
          ar2 <= hi;
        else
          ar1 <= hilo(62 downto 31);
          ar2 <= div_abs_r2;
        end if;
      when others =>
        -- Regular operation adds r1 to r2
        ar1 <= r1;
        ar2 <= r2;
    end case;
  end process;

  process(op, counter)
  begin
    -- Adder mode
    -- The most special mode is division because it has three segments (see above).
    case op is
      when sub_c    => addop <= sub_c;
      when slt_c    => addop <= slt_c;
      when branch_c => addop <= branch_c;
      when div_c    =>
        if counter < 2 then
          addop <= abs_c;
        else
          addop <= sub_c;
        end if;
      when others => addop <= add_c;
    end case;
  end process;

  -- Correctly set output signal
  -- Addition, subtraction, slt and branching are done in the same component.
  -- Multiplication and division have no defined output
  with op select w_int <=
    w_add      when add_c|sub_c|branch_c|slt_c,
    w_or       when or_c,
    hi         when mfhi_c,
    lo         when mflo_c,
    w_lui      when lui_c,
    dontcare32 when mult_c|div_c;

  -- Main process
  process(op_en, clk, rst)
    variable i_hi    : int32;
    variable working : boolean;
    variable ready   : boolean;
  begin
    -- Asynchronous reset
    if rst = '0' then
      rdy     <= '0';
      hilo    <= (63 downto 0 => '0');
      counter <= 0;
      i_hi    := zeroes32;
      working := false;
      ready   := false;
    elsif rising_edge(clk) then
      -- State transisions
      if (not working) and (op_en = '1') then
        -- Transision from idle state to working
        working := true;
        counter <= 0;
        rdy     <= '0';
      elsif (not working) then
        -- Disable ready in the idle state
        rdy <= '0';
      end if;

      -- Normal operation
      if working then
        -- Disable ready
        ready := false;

        -- Execution of operation
        case op is
          when mult_c =>
            -- Multiplication has one pre-state for initialisation
            if counter = 0 then
              lo <= r1;
              hi <= (31 downto 0 => '0');
            else
              -- Conditional addition
              if lo(0) = '1' then
                i_hi := w_add;
              else
                i_hi := hi;
              end if;

              -- Shift
              hilo <= input_sign & i_hi & lo(31 downto 1);
            end if;

            -- Increase counter to keep track of progress
            counter <= counter + 1;

          when div_c =>
            if counter = 0 then
              -- Initialisation step 1:
              -- Set hi to zero.
              -- Calculate the absolute of r1 and store in lo.
              hi <= zero32;
              lo <= w_add;
            elsif counter = 1 then
              -- Initialisation step 2:
              -- Calculate the absolute of r1 and store in div_abs_r2.
              div_abs_r2 <= w_add;
            elsif counter < 34 then
              -- Regular operation (32 cycles):
              -- Performs the division by conditionally shifting
              if w_add(31) = '1' then
                hilo <= hilo(62 downto 0) & '0';
              else
                hilo <= w_add & hilo(30 downto 0) & '1';
              end if;
            elsif counter = 34 then
              -- Finalisation step 1:
              -- Increment lo.
              if input_sign = '1' then
                lo <= w_add;
              end if;
            elsif counter = 35 then
              -- Finalisation step 2:
              -- Increment hi.
              if r1(31) = '1' then
                hi <= w_add;
              end if;
            end if;

            -- Increase counter to keep track of progress
            counter <= counter + 1;

          when others =>                -- Do nothing
        end case;

        -- Set ready flag when applicable
        case op is
          when mult_c => ready := (counter >= 32);
          when div_c  => ready := (counter >= 35);
          when others => ready := true;
        end case;

        -- Transition to idle state when ready
        if ready then
          rdy     <= '1';
          working := false;
	        w <= w_int;
        end if;
      end if;
    end if;
  end process;
end implementation;
