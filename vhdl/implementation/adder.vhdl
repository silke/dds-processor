library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.processor_types.all;

-- Adder
entity alu_adder is
  port (subop : in op_add_t; s, t : in int32; d : out int32);
end alu_adder;

architecture implementation of alu_adder is
begin
  process(subop, s, t)
    variable rx, rt, rd : int32;
    variable c          : integer range 0 to 1;
  begin
    -- Change addition parameters based on operation
    -- Special cases are:
    --   Subtraction: add the negative of t (inverted + 1)
    --   Branch: add t << 2
    --   Abs: conditionally add t or the negative of t
    case subop is
      when sub_c|slt_c => rt := t;
                          rx := ones32;
                          c  := 1;
      when branch_c => rt := t(29 downto 0) & (1 downto 0 => '0');  -- Multiply t by 4.
                       rx := zeroes32;
                       c  := 0;
      when abs_c => rt := t;
                    if rt(31) = '1' then
                      rx := ones32;
                      c  := 1;
                    else
                      rx := zeroes32;
                      c  := 0;
                    end if;
      when others => rt := t;
                     rx := zeroes32;
                     c  := 0;
    end case;

    -- Do the addition
    rd := s + (rt xor rx) + c;

    -- Contitionally assign output
    case subop is
      when slt_c  => d <= zeroes32(31 downto 1) & rd(31);
      when others => d <= rd;
    end case;
  end process;
end implementation;
