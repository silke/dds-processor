library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.processor_types.all;

entity register_map is
  port(clk     : in  std_ulogic;
       rst     : in  std_ulogic;
       -- Decoder connections
       a_r1    : in  uint5;
       a_r2    : in  uint5;
       a_w     : in  uint5;
       we      : in  bit1;
       -- Mux connections
       data_r1 : out int32;
       data_r2 : out int32;
       -- ALU connections
       data_w  : in  int32
       );
end register_map;

architecture datapath of register_map is
  signal regmap : int32RegMap;
begin
  process(clk, rst, a_r1, a_r2)
  begin
    -- async reset
    if rst = '0' then
      regmap <= (others => (31 downto 0 => '0'));
      data_r1 <= zeroes32;
      data_r2 <= zeroes32;
    elsif rising_edge(clk) then
      -- write to rw if enabled
      if we = '1' then
        regmap(to_integer(unsigned(a_w))) <= data_w;
      end if;
      -- read from r1
      data_r1 <= regmap(to_integer(a_r1));
      -- read from r2
      data_r2 <= regmap(to_integer(a_r2));
    end if;
  end process;
end datapath;
