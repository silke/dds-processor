library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.processor_types.all;

entity memory_controller is
  port(clk      : in  std_ulogic;
       rst      : in  std_ulogic;
       -- Memory connections
       m_data_r : in  std_logic_vector(31 downto 0);
       m_data_w : out std_logic_vector(31 downto 0);
       m_addr   : out std_logic_vector(31 downto 0);
       m_write  : out std_ulogic;
       m_read   : out std_ulogic;
       m_ready  : in  std_ulogic;
       -- Decoder connections
       d_data_r : out bit32;
       d_data_w : in  bit32;
       d_addr   : in  bit32;
       d_read   : in  bit1;
       d_write  : in  bit1;
       d_ready  : out bit1
       );
end memory_controller;

architecture implementation of memory_controller is
  -- State machine is divided into states for a read and a write operation
  -- prepare states wait for the memory to be idle
  -- read/write_memory sets the r/w signal and waits for ready signal from the memory
  -- result step signals ready back to the controller
  type state_t is (idle, read_prep, read_mem,  write_prep, write_mem);  --type of state machine.
  signal state : state_t;
begin
  process (clk, rst)
  begin
    if rst = '0' then
      state <= idle;
    elsif (rising_edge(clk)) then
      case state is
        -- Idle, address on line
        when idle =>
          if d_read = '1' and m_ready = '0' then
            state <= read_mem;
          elsif d_read = '1' then
            state <= read_prep;
          elsif d_write = '1' and m_ready = '0' then
            state <= write_prep;
          elsif d_write = '1' then
            state <= write_prep;
          else
            state <= idle;
          end if;
        -- address is on the lines, wait for memory idle
        when read_prep =>
          if m_ready = '0' then
            state <= read_mem;
          else
            state <= read_prep;
          end if;
        -- Memory ready, signal a new read
        when read_mem =>
          if m_ready = '1' then
            state <= idle;
          else
            state <= read_mem;
          end if;
        -- address and data is on the lines, wait for memory idle
        when write_prep =>
          if m_ready = '0' then
            state <= write_mem;
          else
            state <= write_prep;
          end if;
        -- Memory ready, signal a new write
        when write_mem =>
          if m_ready = '1' then
            state <= idle;
          else
            state <= write_mem;
          end if;
      end case;
    end if;
  end process;

  process (state, m_ready)
  begin
    m_read  <= '0';
    m_write <= '0';
    d_ready <= '0';
    case state is
      when idle =>
      when read_prep =>
      when read_mem =>
        m_read  <= '1';
        d_ready <= m_ready;
      when write_prep =>
      when write_mem =>
        m_write <= '1';
        d_ready <= m_ready;
    end case;
  end process;

  d_data_r <= bit32(m_data_r);
  m_data_w <= std_logic_vector(d_data_w);
  m_addr   <= std_logic_vector(d_addr);

end implementation;
