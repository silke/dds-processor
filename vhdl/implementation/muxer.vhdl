library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.processor_types.all;

entity muxer is
  port(pc_dec     : in  int30;  -- Program counter from decoder (current instruction)
       alu_data   : in  int32;  -- Program counter from ALU (where to branch to)
       pc_out     : out int30;          -- Next program counter
       mux_imm    : in  bit1;           -- Switches r2 to imm
       mux_mem    : in  bit1;           -- Switches r1 to mem
       mux_addr   : in  bit1;           -- Switches memory address
       mux_branch : in  mux_branch_t;   -- Type of branch
       reg_r1     : in  int32;          -- Data from register (rs)
       reg_r2     : in  int32;          -- Data from register (rt)
       reg_w      : out int32;          -- Data to register
       imm        : in  int16;          -- Immediate value
       mem        : in  int32;          -- Data from memory
       r1         : out int32;          -- Data to alu: rs | pc
       r2         : out int32;          -- Data to alu: rt | imm
       mem_addr   : out int32);         -- Memory address
end muxer;

architecture implementation of muxer is
  signal pc_next : int30;
begin
  -- Calculate and set the next program counter
  process(mux_branch, reg_r1, reg_r2, alu_data, pc_dec)
    alias neg      : bit1 is reg_r1(31);
    variable pc    : int30;
    variable equal : bit1;
  begin
    -- Check if r1 and r2 are equal
    if reg_r1 = reg_r2 then
      equal := '1';
    else
      equal := '0';
    end if;

    -- Branch if the muxer is set to gez or eq
    -- In case of gez, branch if sign is 0
    -- In case of eq, branch if equal
    if ((mux_branch = b_gez_c) and (neg = '0')) or
      ((mux_branch = b_eq_c) and (equal = '1')) then
      pc := alu_data(31 downto 2);
    else
      pc := pc_dec;
    end if;

    -- Set program counter output
    pc_next <= pc + 1;
  end process;

  -- Set program counter to alu
  pc_out <= pc_next;

  -- Select data to alu
  with mux_branch select r1 <=
    pc_dec & zeroes2 when b_gez_c|b_eq_c,
    reg_r1           when others;

  -- Select the immediate value or data from register
  with mux_imm select r2 <=
    (31 downto 16 => imm(15)) & imm when '1',
    reg_r2                          when others;

  -- Select data from memory or data from alu to write to register
  with mux_mem select reg_w <=
    mem      when '1',
    alu_data when others;

  -- Select address for the memory controller
  mem_addr(1 downto 0) <= "00";
  with mux_addr select mem_addr(31 downto 2) <=
    alu_data(31 downto 2) when '1',
    pc_dec                when others;

end implementation;
