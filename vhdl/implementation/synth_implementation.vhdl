library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.processor_types.all;

entity synth_implementation is
  port(clk         : in  std_ulogic;
       reset       : in  std_ulogic;
       mem_busout  : in  std_logic_vector(31 downto 0);
       mem_ready   : in  std_ulogic;
       mc_m_data_w : out std_logic_vector(31 downto 0);
       mc_m_addr   : out std_logic_vector(31 downto 0);
       mc_m_write  : out std_ulogic;
       mc_m_read   : out std_ulogic);
end synth_implementation;

architecture synth of synth_implementation is

  component muxer
    port(pc_dec     : in  int30;
         alu_data   : in  int32;
         pc_out     : out int30;
         mux_imm    : in  bit1;
         mux_mem    : in  bit1;
         mux_addr   : in  bit1;
         mux_branch : in  mux_branch_t;
         reg_r1     : in  int32;
         reg_r2     : in  int32;
         reg_w      : out int32;
         imm        : in  int16;
         mem        : in  int32;
         r1         : out int32;
         r2         : out int32;
         mem_addr   : out int32);
  end component;

  component alu
    port(clk   : in  std_ulogic;
         rst   : in  std_ulogic;
         op    : in  op_alu_t;
         op_en : in  bit1;
         rdy   : out bit1;
         r1    : in  int32;
         r2    : in  int32;
         w     : out int32);
  end component;

  component register_map
    port(clk     : in  std_ulogic;
         rst     : in  std_ulogic;
         -- Decoder connections
         a_r1    : in  uint5;
         a_r2    : in  uint5;
         a_w     : in  uint5;
         we      : in  bit1;
         -- Mux connections
         data_r1 : out int32;
         data_r2 : out int32;
         -- ALU connections
         data_w  : in  int32);
  end component;

  component decoder
    port(clk        : in  std_ulogic;
         rst        : in  std_ulogic;
         -- Memory controller connections
         mc_data_r  : in  bit32;
         mc_read    : out bit1;
         mc_write   : out bit1;
         mc_ready   : in  bit1;
         -- Program counters
         pc_in      : in  int30;
         pc_out     : out int30;
         -- Register connections
         reg_read_1 : out bit5;
         reg_read_2 : out bit5;
         reg_write  : out bit5;
         reg_we     : out bit1;
         -- Mux connections
         mux_imm    : out bit1;
         mux_mem    : out bit1;
         mux_branch : out mux_branch_t;
         mux_addr   : out bit1;
         imm        : out bit16;
         -- ALU connections
         alu_op     : out op_alu_t;
         alu_enable : out bit1;
         alu_rdy    : in  bit1);
  end component;

  component memory_controller
    port(clk      : in  std_ulogic;
         rst      : in  std_ulogic;
         -- Memory connections
         m_data_r : in  std_logic_vector(31 downto 0);
         m_data_w : out std_logic_vector(31 downto 0);
         m_addr   : out std_logic_vector(31 downto 0);
         m_write  : out std_ulogic;
         m_read   : out std_ulogic;
         m_ready  : in  std_ulogic;
         -- Decoder connections
         d_data_r : out bit32;
         d_data_w : in  bit32;
         d_addr   : in  bit32;
         d_read   : in  bit1;
         d_write  : in  bit1;
         d_ready  : out bit1);
  end component;

  -- Connections from behavioural processor
  signal cpu_busout, cpu_addr : bit32;
  signal cpu_write, cpu_read  : std_ulogic;

  -- Connections from muxer
  signal mux_pc                           : int30;
  signal mux_r1, mux_r2, mux_rw, mux_addr : int32;

  -- Connections from alu
  signal alu_rdy  : bit1;
  signal alu_data : int32;

  -- Connections from register
  signal reg_r1, reg_r2 : int32;

  -- Connections from decoder
  signal dec_pc                                                         : int30;
  signal dec_imm                                                        : bit16;
  signal dec_reg_read_1, dec_reg_read_2, dec_reg_write                  : bit5;
  signal dec_mc_read, dec_mc_write                                      : bit1;
  signal dec_reg_we, dec_mux_imm, dec_mux_mem, dec_mux_addr, dec_alu_en : bit1;
  signal dec_mux_branch                                                 : mux_branch_t;
  signal dec_alu_op                                                     : op_alu_t;

  -- Connections from memory controller
  signal mc_data                : bit32;
  signal mc_ready               : bit1;

begin
  mc : memory_controller
    port map(clk,
             reset,
             mem_busout,
             mc_m_data_w,
             mc_m_addr,
             mc_m_write,
             mc_m_read,
             mem_ready,
             mc_data,
             bit32(reg_r2),
             bit32(mux_addr),
             dec_mc_read,
             dec_mc_write,
             mc_ready);

  dec : decoder
    port map(clk,
             reset,
             bit32(mc_data),
             dec_mc_read,
             dec_mc_write,
             mc_ready,
             mux_pc,
             dec_pc,
             dec_reg_read_1,
             dec_reg_read_2,
             dec_reg_write,
             dec_reg_we,
             dec_mux_imm,
             dec_mux_mem,
             dec_mux_branch,
             dec_mux_addr,
             dec_imm,
             dec_alu_op,
             dec_alu_en,
             alu_rdy);

  mux : muxer
    port map(dec_pc,
             alu_data,
             mux_pc,
             dec_mux_imm,
             dec_mux_mem,
             dec_mux_addr,
             dec_mux_branch,
             reg_r1,
             reg_r2,
             mux_rw,
             int16(dec_imm),
             int32(mc_data),
             mux_r1,
             mux_r2,
             mux_addr);

  t_alu : alu
    port map(clk,
             reset,
             dec_alu_op,
             dec_alu_en,
             alu_rdy,
             mux_r1,
             mux_r2,
             alu_data);

  reg : register_map
    port map(clk,
             reset,
             uint5(dec_reg_read_1),
             uint5(dec_reg_read_2),
             uint5(dec_reg_write),
             dec_reg_we,
             reg_r1,
             reg_r2,
             mux_rw);

end synth;
