library vunit_lib;
library osvvm;
library ieee;

use osvvm.RandomPkg.all;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.processor_types.all;
--use work.subtractor.all;
use vunit_lib.lang.all;
use vunit_lib.string_ops.all;
use vunit_lib.dictionary.all;
use vunit_lib.path.all;
use vunit_lib.log_types_pkg.all;
use vunit_lib.log_special_types_pkg.all;
use vunit_lib.log_pkg.all;
use vunit_lib.check_types_pkg.all;
use vunit_lib.check_special_types_pkg.all;
use vunit_lib.check_pkg.all;
use vunit_lib.run_types_pkg.all;
use vunit_lib.run_special_types_pkg.all;
use vunit_lib.run_base_pkg.all;
use vunit_lib.run_pkg.all;


entity tb_muxer is
  generic (runner_cfg : string := "");
end tb_muxer;

architecture tb of tb_muxer is
  component muxer
    port(pc_dec     : in  int30;
         alu_data   : in  int32;
         pc_out     : out int30;
         mux_imm    : in  bit1;
         mux_mem    : in  bit1;
         mux_addr   : in  bit1;
         mux_branch : in  mux_branch_t;
         reg_r1     : in  int32;
         reg_r2     : in  int32;
         reg_w      : out int32;
         imm        : in  int16;
         mem        : in  int32;
         r2         : out int32;
         mem_addr   : out int32);
  end component;

  signal mux_pc, pc_dec                                     : int30;
  signal r2, alu_data, reg_r1, reg_r2, reg_w, mem, mem_addr : int32;
  signal imm                                                : int16;
  signal mux_branch                                         : mux_branch_t;
  signal clk, mux_imm, mux_mem, mux_addr                    : bit1 := '0';
begin
  t_mux : muxer
    port map (pc_dec, alu_data, mux_pc, mux_imm, mux_mem, mux_addr, mux_branch, reg_r1, reg_r2, reg_w, imm, mem, r2, mem_addr);

  clk <= not clk after 10 ns;

  main : process
    variable RV1, RV2, RV3, RV4, RV5, RV6, RV7, RV8, RV9 : RandomPType;
    variable branch                                      : integer;
  begin
    -- Setup simulation
    test_runner_setup(runner, runner_cfg);

    -- Generate initial seeds
    RV1.InitSeed (RV1'instance_name);
    RV2.InitSeed (RV2'instance_name);
    RV3.InitSeed (RV3'instance_name);
    RV4.InitSeed (RV4'instance_name);
    RV5.InitSeed (RV5'instance_name);
    RV6.InitSeed (RV6'instance_name);
    RV7.InitSeed (RV7'instance_name);
    RV8.InitSeed (RV8'instance_name);
    RV9.InitSeed (RV9'instance_name);

    -- Wait for clock
    wait until clk = '1';

    -- Test with random values
    for i in 0 to int16_max loop
      -- Random data
      reg_r1 <= RV1.RandSigned(int32_min, int32_max, int32'length);
      reg_r2 <= RV2.RandSigned(int32_min, int32_max, int32'length);
      mem    <= RV3.RandSigned(int32_min, int32_max, int32'length);
      imm    <= RV2.RandSigned(int16_min, int16_max, int16'length);

      -- Random PC's
      pc_dec   <= RV4.RandSigned(int30'low, int30'high, int30'length);
      alu_data <= RV5.RandSigned(int30'low, int30'high, int30'length) & zeroes2;

      -- Random muxes
      mux_imm  <= RV6.RandUnsigned(0, 1, 1)(0);
      mux_mem  <= RV7.RandUnsigned(0, 1, 1)(0);
      mux_addr <= RV8.RandUnsigned(0, 1, 1)(0);
      branch   := RV9.RandInt(0, 2);

      -- Set branch
      case branch is
        when 0      => mux_branch <= b_eq_c;
        when 1      => mux_branch <= b_gez_c;
        when others => mux_branch <= b_none_c;
      end case;

      wait until clk = '1';

      -- Check mux_imm
      if mux_imm = '1' then
        check(r2(15 downto 0) = imm, "mux_imm=1 incorrect", failure);
      else
        check(r2 = reg_r2, "mux_imm=0 incorrect", failure);
      end if;

      -- Check mux_mem
      if mux_mem = '1' then
        check(reg_w = mem, "mux_mem=1 incorrect", failure);
      else
        check(reg_w = alu_data, "mux_mem=0 incorrect", failure);
      end if;

      -- Check mux_addr
      if mux_addr = '1' then
        check(mem_addr = alu_data, "mux_addr=1 incorrect", failure);
      else
        check(mem_addr = pc_dec & zeroes2, "mux_addr=0 incorrect", failure);
      end if;

      -- Check branch
      case mux_branch is
        when b_eq_c =>
          if reg_r1 = reg_r2 then
            check(mux_pc = alu_data(31 downto 2) + 1, "beq = true incorrect", failure);
          else
            check(mux_pc = pc_dec + 1, "beq = false incorrect", failure);
          end if;

        when b_gez_c =>
          if reg_r1 >= 0 then
            check(mux_pc = alu_data(31 downto 2) + 1, "bgez = true incorrect", failure);
          else
            check(mux_pc = pc_dec + 1, "bgez = false incorrect", failure);
          end if;

        when others =>
          check(mux_pc = pc_dec + 1, "no branch incorrect", failure);
      end case;
    end loop;

    -- Simulation ends here
    test_runner_cleanup(runner);
  end process;
end tb;
