library vunit_lib;
use vunit_lib.lang.all;
use vunit_lib.string_ops.all;
use vunit_lib.dictionary.all;
use vunit_lib.path.all;
use vunit_lib.log_types_pkg.all;
use vunit_lib.log_special_types_pkg.all;
use vunit_lib.log_pkg.all;
use vunit_lib.check_types_pkg.all;
use vunit_lib.check_special_types_pkg.all;
use vunit_lib.check_pkg.all;
use vunit_lib.run_types_pkg.all;
use vunit_lib.run_special_types_pkg.all;
use vunit_lib.run_base_pkg.all;
use vunit_lib.run_pkg.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.processor_types.all;
use work.memory_config.all;

entity tb_impl_memory is
  generic (runner_cfg : string := "");
end tb_impl_memory;

architecture memory_control of tb_impl_memory is

  component memory
    generic (tpd : time := 1 ns);
    port(d_busout : out std_logic_vector(31 downto 0);
         d_busin  : in  std_logic_vector(31 downto 0);
         a_bus    : in  std_logic_vector(31 downto 0);
         clk      : in  std_ulogic;
         write    : in  std_ulogic;
         read     : in  std_ulogic;
         ready    : out std_ulogic);
  end component;

  component memory_controller
    port(clk      : in  std_ulogic;
         rst      : in  std_ulogic;
         -- Memory connections
         m_data_r : in  std_logic_vector(31 downto 0);
         m_data_w : out std_logic_vector(31 downto 0);
         m_addr   : out std_logic_vector(31 downto 0);
         m_write  : out std_ulogic;
         m_read   : out std_ulogic;
         m_ready  : in  std_ulogic;
         -- Decoder connections
         d_data_r : out bit32;
         d_data_w : in  bit32;
         d_addr   : in  bit32;
         d_read   : in  bit1;
         d_write  : in  bit1;
         d_ready  : out bit1
         );
  end component;

  signal mem_data_r, mem_data_w, mem_addr     : std_logic_vector(31 downto 0);
  signal tb_busin, tb_busout, tb_addr,tb_testread  : bit32 := ( others => '0');
  signal tb_r, tb_w, tb_rdy                   : bit1 := '0';
  signal mem_r, mem_w, mem_rdy                 : std_ulogic := '0';
  signal reset                                : std_ulogic := '0';
  signal clk                                  : std_ulogic := '0';

begin
  control : memory_controller
    port map(clk,
             reset,
             mem_data_r,
             mem_data_w,
             mem_addr,
             mem_w,
             mem_r,
             mem_rdy,
             tb_busin,
             tb_busout,
             tb_addr,
             tb_r,
             tb_w,
             tb_rdy);

  mem_impl : memory
    generic map (1 ns)
    port map (mem_data_r,
              mem_data_w,
              mem_addr,
              clk,
              mem_w,
              mem_r,
              mem_rdy);

  reset <= '0', '1' after 100 ns;
  clk   <= not clk  after 10 ns;
  main : process
    variable testread :bit32 := (others => '0'); 
  begin
    test_runner_setup(runner, runner_cfg);
    -- Wait for reset
    wait for 150 ns;
    for i in 0 to 20 loop
      -- Wait for initial clock tick and set address
      wait until clk = '1';
      tb_addr <= bit32(to_unsigned(text_base_address + i*4, bit32'length));
      log("Setting addr to " & natural'image(to_integer(unsigned(tb_addr))));
      -- Strobe read for a clock tick
      tb_r <= '1';
      wait until clk = '1';
      tb_r <= '0';
      -- Wait until memory ready.
      loop
        wait until clk = '1';
        if tb_rdy = '1' then
          testread := tb_busin;
          exit;
        end if;
      end loop;
      -- Check if the results match
      tb_addr <= (others => '0');
      log("Got " & natural'image(to_integer(unsigned(testread))) & " from controller"); 
      log("Got " & natural'image(to_integer(unsigned(mem_data_r))) & " from memory"); 
      check(bit32(mem_data_r) = testread, "data does not match", failure);
    end loop;
    test_runner_cleanup(runner);        -- Simulation ends here
  end process;
end memory_control;
