library vunit_lib;
use vunit_lib.lang.all;
use vunit_lib.string_ops.all;
use vunit_lib.dictionary.all;
use vunit_lib.path.all;
use vunit_lib.log_types_pkg.all;
use vunit_lib.log_special_types_pkg.all;
use vunit_lib.log_pkg.all;
use vunit_lib.check_types_pkg.all;
use vunit_lib.check_special_types_pkg.all;
use vunit_lib.check_pkg.all;
use vunit_lib.run_types_pkg.all;
use vunit_lib.run_special_types_pkg.all;
use vunit_lib.run_base_pkg.all;
use vunit_lib.run_pkg.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.processor_types.all;
use work.memory_config.all;

entity tb_impl_reg is
  generic (runner_cfg : string := "");
end tb_impl_reg;

architecture impl_reg of tb_impl_reg is

  component register_map
    generic (tpd : time := 1 ns);
      port(clk     : in  std_ulogic;
           rst     : in  std_ulogic;
           -- Decoder connections
           a_r1    : in  uint5;
           a_r2    : in  uint5;
           a_w     : in  uint5;
           we      : in  bit1;
           -- Mux connections
           data_r1 : out int32;
           data_r2 : out int32;
           -- ALU connections
           data_w  : in  int32
           );
  end component;
  
  signal reg_r1, reg_r2, reg_w                : int32 := zeroes32;
  signal addr_r1, addr_r2, addr_w             : uint5 := (others => '0');
  signal sig_we                               : std_ulogic := '0';
  signal reset                                : std_ulogic := '0';
  signal clk                                  : std_ulogic := '0';

begin
  reg_map : register_map
    port map(clk,
             reset,
             addr_r1,
             addr_r2,
             addr_w,
             sig_we,
             reg_r1,
             reg_r2,
             reg_w
             );

  reset <= '0', '1' after 100 ns;
  clk   <= not clk  after 10 ns;
  main : process
  begin
    test_runner_setup(runner, runner_cfg);
    -- Wait for reset
    wait for 150 ns;
    for i in 1 to 10 loop

      -- Set output registers
      addr_r1 <= to_unsigned(i, uint5'length);
      addr_r2 <= to_unsigned(i+12, uint5'length); 
      wait until clk = '1';

      -- Write value to one register
      addr_w <=  to_unsigned(i, uint5'length);
      reg_w <= to_signed(i*16, int32'length);
      sig_we <= '1';
      wait until clk = '1';
      sig_we <= '0';
      wait until clk = '1';
      
      -- Write value to second register
      addr_w <=  to_unsigned(i+12, uint5'length);
      reg_w <= to_signed(i*4, int32'length);
      sig_we <= '1';
      wait until clk = '1';
      sig_we <= '0';
      wait for 100 ns;
      
      -- Check if the reads match with expected
      check(reg_r1 = i*16, "register data does not match, got: " & natural'image(to_integer(reg_r1)) & " expected: " & natural'image(i*16) , failure);
      check(reg_r2 = i*4, "register data does not match, got: " & natural'image(to_integer(reg_r2)) & " expected: " & natural'image(i*4),  failure);
    end loop;
    test_runner_cleanup(runner);        -- Simulation ends here
  end process;
end impl_reg;
