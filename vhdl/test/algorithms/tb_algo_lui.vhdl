library vunit_lib;
use vunit_lib.lang.all;
use vunit_lib.string_ops.all;
use vunit_lib.dictionary.all;
use vunit_lib.path.all;
use vunit_lib.log_types_pkg.all;
use vunit_lib.log_special_types_pkg.all;
use vunit_lib.log_pkg.all;
use vunit_lib.check_types_pkg.all;
use vunit_lib.check_special_types_pkg.all;
use vunit_lib.check_pkg.all;
use vunit_lib.run_types_pkg.all;
use vunit_lib.run_special_types_pkg.all;
use vunit_lib.run_base_pkg.all;
use vunit_lib.run_pkg.all;
library osvvm;
library ieee;

use osvvm.RandomPkg.all;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.processor_types.all;
use work.logger.all;
use work.load_upper_i.all;

entity tb_algo_lui is
  generic (runner_cfg : string := "");
end tb_algo_lui;

architecture tb of tb_algo_lui is
begin
  process
    variable RV1              : RandomPType;
    variable in_i             : bit16;
    variable algo1_r, behav_r : integer;
  begin
    -- Setup simlation
    test_runner_setup(runner, runner_cfg);

    -- Generate initial seeds
    RV1.InitSeed (RV1'instance_name);

    -- Test with random values
    for i in 0 to uint16_max loop
      -- Generate random values
      in_i := bit16(to_signed(RV1.RandInt(int16_min, int16_max), bit16'length));

      -- Runn all descriptions
      behav_r := behav_lui(in_i);
      algo1_r := algof_lui(in_i);

      -- Check results
      check(algo1_r = behav_r,
            "Lui algo failed : " & std_to_string(in_i) &
            " => " & integer'image(behav_r) &
            " got: " & integer'image(algo1_r) , failure);
    end loop;

    -- Simulation ends here
    test_runner_cleanup(runner);
  end process;
end tb;
