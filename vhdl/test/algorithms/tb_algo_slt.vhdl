library vunit_lib;
use vunit_lib.lang.all;
use vunit_lib.string_ops.all;
use vunit_lib.dictionary.all;
use vunit_lib.path.all;
use vunit_lib.log_types_pkg.all;
use vunit_lib.log_special_types_pkg.all;
use vunit_lib.log_pkg.all;
use vunit_lib.check_types_pkg.all;
use vunit_lib.check_special_types_pkg.all;
use vunit_lib.check_pkg.all;
use vunit_lib.run_types_pkg.all;
use vunit_lib.run_special_types_pkg.all;
use vunit_lib.run_base_pkg.all;
use vunit_lib.run_pkg.all;
library osvvm;
library ieee;

use osvvm.RandomPkg.all;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.processor_types.all;
use work.less_than.all;

entity tb_algo_slt is
  generic (runner_cfg : string := "");
end tb_algo_slt;

architecture tb of tb_algo_slt is
begin
  process
    variable RV1, RV2                        : RandomPType;
    variable in_s, in_t                      : integer;
    variable algo1_res, behav_res : integer;
  begin
    -- Setup simulation
    test_runner_setup(runner, runner_cfg);

    -- Generate initial seeds
    RV1.InitSeed (RV1'instance_name);
    RV2.InitSeed (RV2'instance_name);

    -- Test with random values
    for i in 0 to uint16_max loop
      -- Generate random values
      in_s := RV1.RandInt(int32_min/2, int32_max/2);
      in_t := RV2.RandInt(int32_min/2, int32_max/2);

      -- Runn all descriptions
      behav_res := behav_slt(in_s, in_t);
      algo1_res := algof_slt(in_s, in_t);

      -- Check results
      check(algo1_res = behav_res, "slt algo 1 failed : " & integer'image(in_s) & " - " & integer'image(in_t) & " = " & integer'image(behav_res) & " got: " & integer'image(algo1_res) , failure);
    end loop;

    -- Simulation ends here
    test_runner_cleanup(runner);
  end process;
end tb;
