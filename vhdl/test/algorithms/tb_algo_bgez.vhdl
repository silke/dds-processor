library vunit_lib;
use vunit_lib.lang.all;
use vunit_lib.string_ops.all;
use vunit_lib.dictionary.all;
use vunit_lib.path.all;
use vunit_lib.log_types_pkg.all;
use vunit_lib.log_special_types_pkg.all;
use vunit_lib.log_pkg.all;
use vunit_lib.check_types_pkg.all;
use vunit_lib.check_special_types_pkg.all;
use vunit_lib.check_pkg.all;
use vunit_lib.run_types_pkg.all;
use vunit_lib.run_special_types_pkg.all;
use vunit_lib.run_base_pkg.all;
use vunit_lib.run_pkg.all;
library osvvm;
library ieee;

use osvvm.RandomPkg.all;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.processor_types.all;
use work.branch_gez.all;


entity tb_algo_bgez is
  generic (runner_cfg : string := "");
end tb_algo_bgez;

architecture tb of tb_algo_bgez is
begin
  process
    variable RV1, RV2, RV3                   : RandomPType;
    variable same, in_s, in_t, in_i, in_pc   : integer;
    variable algo1_npc, algo2_npc, behav_npc : integer;
  begin
    test_runner_setup(runner, runner_cfg);

    -- Generate initial seeds
    RV1.InitSeed (RV1'instance_name);
    RV2.InitSeed (RV2'instance_name);
    RV3.InitSeed (RV3'instance_name);

    -- Test with random values
    for i in 0 to 5000 loop
      -- Generate random values
      in_s  := RV1.RandInt(int32_min, int32_max);
      in_i  := RV2.RandInt(int16_min, int16_max);
      in_pc := RV3.RandInt(0, int32_max);

      -- Runn all descriptions
      behav_npc := behav_bgez(in_s, in_i, in_pc);
      algo1_npc := algof_bgez(in_s, in_i, in_pc);

      -- Check results
      check(algo1_npc = behav_npc,
            "Branch >= 0 algo failed : " & integer'image(in_s) &
            " >=0 (" & integer'image(in_i) & ") => " & integer'image(behav_npc) &
            " got: " & integer'image(algo1_npc) , failure);
    end loop;
    test_runner_cleanup(runner);        -- Simulation ends here
  end process;
end tb;
