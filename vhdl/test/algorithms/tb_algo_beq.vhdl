library vunit_lib;
use vunit_lib.lang.all;
use vunit_lib.string_ops.all;
use vunit_lib.dictionary.all;
use vunit_lib.path.all;
use vunit_lib.log_types_pkg.all;
use vunit_lib.log_special_types_pkg.all;
use vunit_lib.log_pkg.all;
use vunit_lib.check_types_pkg.all;
use vunit_lib.check_special_types_pkg.all;
use vunit_lib.check_pkg.all;
use vunit_lib.run_types_pkg.all;
use vunit_lib.run_special_types_pkg.all;
use vunit_lib.run_base_pkg.all;
use vunit_lib.run_pkg.all;
library osvvm;
library ieee;

use osvvm.RandomPkg.all;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.processor_types.all;
use work.branch_eq.all;

entity tb_algo_beq is
  generic (runner_cfg : string := "");
end tb_algo_beq;

architecture tb of tb_algo_beq is
begin
  process
    variable RV0, RV1, RV2, RV3, RV4         : RandomPType;
    variable same, in_s, in_t, in_i, in_pc   : integer;
    variable algo1_npc, algo2_npc, behav_npc : integer;
  begin
    test_runner_setup(runner, runner_cfg);

    -- Generate initial seeds
    RV0.InitSeed (RV0'instance_name);
    RV1.InitSeed (RV1'instance_name);
    RV2.InitSeed (RV2'instance_name);
    RV3.InitSeed (RV3'instance_name);
    RV4.InitSeed (RV4'instance_name);

    -- Test with random values
    for i in 0 to 5000 loop
      -- Generate random values
      same  := RV0.RandInt(0, 1);
      in_s  := RV1.RandInt(int32_min, int32_max);
      in_t  := RV2.RandInt(int32_min, int32_max);
      in_i  := RV3.RandInt(int16_min, int16_max);
      in_pc := RV4.RandInt(0, int32_max);

      -- Make them the same 50 percent of the time
      if same = 1 then
        in_t := in_s;
      end if;

      -- Runn all descriptions
      behav_npc := behav_beq(in_s, in_t, in_i, in_pc);
      algo1_npc := algo1_beq(in_s, in_t, in_i, in_pc);
      algo2_npc := algo2_beq(in_s, in_t, in_i, in_pc);

      -- Check results
      check(algo1_npc = behav_npc, "Branch if equal algo 1 failed : " & integer'image(in_s) & " ?= " & integer'image(in_t) & " (" & integer'image(in_i) & ") => " & integer'image(behav_npc) & " got: " & integer'image(algo1_npc) , failure);
      check(algo2_npc = behav_npc, "Branch if equal algo 2 failed : " & integer'image(in_s) & " ?= " & integer'image(in_t) & " (" & integer'image(in_i) & ") => " & integer'image(behav_npc) & " got: " & integer'image(algo2_npc) , failure);
    end loop;
    test_runner_cleanup(runner);        -- Simulation ends here
  end process;
end tb;
