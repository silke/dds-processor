library vunit_lib;
use vunit_lib.lang.all;
use vunit_lib.string_ops.all;
use vunit_lib.dictionary.all;
use vunit_lib.path.all;
use vunit_lib.log_types_pkg.all;
use vunit_lib.log_special_types_pkg.all;
use vunit_lib.log_pkg.all;
use vunit_lib.check_types_pkg.all;
use vunit_lib.check_special_types_pkg.all;
use vunit_lib.check_pkg.all;
use vunit_lib.run_types_pkg.all;
use vunit_lib.run_special_types_pkg.all;
use vunit_lib.run_base_pkg.all;
use vunit_lib.run_pkg.all;
library osvvm;
library ieee;

use osvvm.RandomPkg.all;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.processor_types.all;
use work.multiplier.all;

entity tb_algo_mult is
  generic (runner_cfg : string := "");
end tb_algo_mult;

architecture tb of tb_algo_mult is
begin
  process
    variable RV1, RV2                         : RandomPType;
    variable in1, in2, hi, lo, exp_hi, exp_lo : integer;
    variable t1, t2 : bit6;
    constant lo_max : integer := 954437177;
    constant hi_max: integer  := 477218588;
    constant lo_min : integer := -477218588;
    constant hi_min : integer := 477218588;

    constant mult_max   : integer := 1431655765;
    constant mult_min   : integer := -1431655766;
  begin
    -- Setup simulation
    test_runner_setup(runner, runner_cfg);

    -- Generate initial seeds
    RV1.InitSeed (RV1'instance_name);
    RV2.InitSeed (RV2'instance_name);

    -- Check multiplication with maximum values
    behav_mult(mult_max, mult_max, exp_hi, exp_lo);
    algo1_mult(mult_max, mult_max, hi, lo);

    check(lo = lo_max,
          "lo upper boundary failed: " & integer'image(lo_max) &
          " got: " & integer'image(exp_lo) & ", " & integer'image(lo),
          failure);
    check(hi = hi_max,
          "hi upper boundary failed: " & integer'image(hi_max) &
          " got: " & integer'image(exp_hi) & ", " & integer'image(hi),
          failure);

    -- Check multiplication with minimum values
    behav_mult(mult_min, mult_min, exp_hi, exp_lo);
    algo1_mult(mult_min, mult_min, hi, lo);

    -- Lower boundary
    check(lo = lo_min or exp_lo = lo_min,
          "lo lower boundary failed: " & integer'image(lo_min) &
          " got: " & integer'image(exp_lo) & ", " & integer'image(lo),
          failure);
    check(hi = hi_min or exp_hi = hi_min,
          "hi lower boundary failed: " & integer'image(hi_min) &
          " got: " & integer'image(exp_hi) & ", " & integer'image(hi),
          failure);

    -- Test with random samples
    for i in 0 to uint16_max loop
      -- Generate random values
      in1 := RV1.RandInt(0, mult_max);
      in2 := RV2.RandInt(0, mult_max);

      -- Run algorithm
      behav_mult(in1, in2, exp_hi, exp_lo);
      algo1_mult(in1, in2, hi, lo);

      -- Check results
      check(exp_lo = lo,
            "multiplication failed at lo: " &
            integer'image(in1) & " * " & integer'image(in2) &
            " = " & integer'image(exp_lo) &
            " got: " & integer'image(lo), failure);
      check(exp_hi = hi,
            "multiplication failed at hi: " &
            integer'image(in1) & " * " & integer'image(in2) &
            " = " & integer'image(exp_hi) &
            " got: " & integer'image(hi), failure);
    end loop;

    -- Simulation ends here
    test_runner_cleanup(runner);
  end process;
end tb;
