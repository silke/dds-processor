library vunit_lib;
use vunit_lib.lang.all;
use vunit_lib.string_ops.all;
use vunit_lib.dictionary.all;
use vunit_lib.path.all;
use vunit_lib.log_types_pkg.all;
use vunit_lib.log_special_types_pkg.all;
use vunit_lib.log_pkg.all;
use vunit_lib.check_types_pkg.all;
use vunit_lib.check_special_types_pkg.all;
use vunit_lib.check_pkg.all;
use vunit_lib.run_types_pkg.all;
use vunit_lib.run_special_types_pkg.all;
use vunit_lib.run_base_pkg.all;
use vunit_lib.run_pkg.all;
library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.processor_types.all;

entity tb_implementation is
  generic (runner_cfg : string := "");
end tb_implementation;

architecture memory_processor of tb_implementation is

  component memory
    port(d_busout : out std_logic_vector(31 downto 0);
         d_busin  : in  std_logic_vector(31 downto 0);
         a_bus    : in  std_logic_vector(31 downto 0);
         clk      : in  std_ulogic;
         write    : in  std_ulogic;
         read     : in  std_ulogic;
         ready    : out std_ulogic);
  end component;

  component processor
    port (d_busout : out bit32;
          d_busin  : in  bit32;
          a_bus    : out bit32;
          write    : out std_ulogic;
          read     : out std_ulogic;
          ready    : in  std_ulogic;
          reset    : in  std_ulogic;
          clk      : in  std_ulogic);
  end component;


  component synth_implementation is
    port(clk         : in  std_ulogic;
         reset       : in  std_ulogic;
         mem_busout  : in  std_logic_vector(31 downto 0);
         mem_ready   : in  std_ulogic;
         mc_m_data_w : out std_logic_vector(31 downto 0);
         mc_m_addr   : out std_logic_vector(31 downto 0);
         mc_m_write  : out std_ulogic;
         mc_m_read   : out std_ulogic);
  end component;


  -- Connections from behavioural processor
  signal cpu_busout, cpu_addr : bit32;
  signal cpu_write, cpu_read  : std_ulogic;

  -- Connections from behavioural memory
  signal mem_busout : std_logic_vector(31 downto 0);
  signal mem_ready  : std_ulogic;

  -- Connections from memory controller
  signal synth_m_data_w, synth_m_addr : std_logic_vector(31 downto 0);
  signal synth_m_write, synth_m_read  : std_ulogic;

  -- Connections to play with
  signal test_mem_ready : bit1;

  -- Reset & clock
  signal reset : std_ulogic := '0';
  signal clk   : std_ulogic := '0';

begin
  cpu : processor
    port map(cpu_busout,
             bit32(mem_busout),
             cpu_addr,
             cpu_write,
             cpu_read,
             test_mem_ready,
             reset,
             clk);

  mem : memory
    port map (mem_busout,
              synth_m_data_w,              -- Checked against cpu_busout
              synth_m_addr,                -- Checked against cpu_addr
              clk,
              synth_m_write,               -- Checked against cpu_write
              synth_m_read,                -- Checked against cpu_read
              mem_ready);

  synthesizable : synth_implementation
    port map(clk,
             reset,
             mem_busout,
             mem_ready,
             synth_m_data_w,
             synth_m_addr,
             synth_m_write,
             synth_m_read
             );

  reset <= '0', '1' after 100 ns;
  clk   <= not clk  after 7 ns;

  test_mem_ready <= mem_ready;

  main : process
  begin
    -- Test setup
    test_runner_setup(runner, runner_cfg);
    wait for 100 ns;
    -- Loop until the write instruction has been reached
    while synth_m_write = '0' loop
      -- Wait for the memory to be ready
      wait until test_mem_ready = '1';

      -- Check implementation vs behavioural
      check(bit32(synth_m_addr) = cpu_addr, "mc_m_addr does not match cpu_addr", failure);
      check(std_ulogic(synth_m_write) = cpu_write, "mc_m_write does not match cpu_write", failure);
      check(std_ulogic(synth_m_read) = cpu_read, "mc_m_read does not match cpu_read", failure);
    end loop;

    -- Check if the correct answer will be written
    check(bit32(synth_m_data_w) = cpu_busout, "mc_m_data_w does not match cpu_busout", failure);
    check(int32(synth_m_data_w) = to_signed(368, bit32'length), "Implementation: invalid answer written to memory.", failure);

    -- Simulation ends here
    test_runner_cleanup(runner);
  end process;
end memory_processor;
