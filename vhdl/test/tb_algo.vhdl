library vunit_lib;
library vunit_lib;
use vunit_lib.lang.all;
use vunit_lib.string_ops.all;
use vunit_lib.dictionary.all;
use vunit_lib.path.all;
use vunit_lib.log_types_pkg.all;
use vunit_lib.log_special_types_pkg.all;
use vunit_lib.log_pkg.all;
use vunit_lib.check_types_pkg.all;
use vunit_lib.check_special_types_pkg.all;
use vunit_lib.check_pkg.all;
use vunit_lib.run_types_pkg.all;
use vunit_lib.run_special_types_pkg.all;
use vunit_lib.run_base_pkg.all;
use vunit_lib.run_pkg.all;
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.processor_types.all;

entity tb_algo is
  generic (runner_cfg : string := "");
end tb_algo;

architecture memory_processor of tb_algo is
  -- for cpu_behav            : processor use entity work.processor(behaviour);
  -- for cpu_algor            : processor use entity work.processor(behaviour_framework);
  -- for mem_behav, mem_algor : memory use entity work.memory(behaviour);

  component memory
    generic (tpd : time := 1 ns);
    port(d_busout : out std_logic_vector(31 downto 0);
         d_busin  : in  std_logic_vector(31 downto 0);
         a_bus    : in  std_logic_vector(31 downto 0);
         clk      : in  std_ulogic;
         write    : in  std_ulogic;
         read     : in  std_ulogic;
         ready    : out std_ulogic);
  end component;

  component processor
    port (d_busout : out bit32;
          d_busin  : in  bit32;
          a_bus    : out bit32;
          write    : out std_ulogic;
          read     : out std_ulogic;
          ready    : in  std_ulogic;
          reset    : in  std_ulogic;
          clk      : in  std_ulogic);
  end component;

  signal behav_data_from_cpu, behav_addr      : bit32;
  signal algor_data_from_cpu, algor_addr      : bit32;
  signal behav_data_to_cpu, algor_data_to_cpu : std_logic_vector(31 downto 0);
  signal behav_read, behav_write, behav_ready : std_ulogic;
  signal algor_read, algor_write, algor_ready : std_ulogic;
  signal reset                                : std_ulogic := '0';
  signal clk                                  : std_ulogic := '0';

begin
  cpu_behav : processor
    port map(behav_data_from_cpu,
             bit32(behav_data_to_cpu),
             behav_addr,
             behav_write,
             behav_read,
             behav_ready,
             reset,
             clk);

  cpu_algor : processor
    port map(algor_data_from_cpu,
             bit32(algor_data_to_cpu),
             algor_addr,
             algor_write,
             algor_read,
             algor_ready,
             reset,
             clk);

  mem_behav : memory
    generic map (1 ns)
    port map (behav_data_to_cpu,
              std_logic_vector(behav_data_from_cpu),
              std_logic_vector(behav_addr),
              clk,
              behav_write,
              behav_read,
              behav_ready);

  mem_algor : memory
    generic map (1 ns)
    port map (algor_data_to_cpu,
              std_logic_vector(algor_data_from_cpu),
              std_logic_vector(algor_addr),
              clk,
              algor_write,
              algor_read,
              algor_ready);

  reset <= '0', '1' after 100 ns;
  clk   <= not clk  after 10 ns;
  main : process
  begin
    test_runner_setup(runner, runner_cfg);
    loop
      wait until clk = '1';
      if behav_read = '1' then
        check(algor_read = '1', "Read timing mismatch between behavioural and algorithmic", failure);
        check(algor_addr = behav_addr, "Read address mismatch between behavioural and algorithmic", failure);
      end if;
      if behav_write = '1' then
        check(algor_write = '1', "Read timing mismatch between behavioural and algorithmic", failure);
        check(algor_data_from_cpu = behav_data_from_cpu, "Write data mismatch between behavioural and algorithmic", failure);
        -- writing is the last instruction happening before the nop
        exit;
      end if;
    end loop;
    check(to_integer(unsigned(algor_data_from_cpu)) = 368, "Invalid answer writen to memory", failure);
    check(to_integer(unsigned(behav_data_from_cpu)) = 368, "Invalid behavioural answer writen to memory", failure);
    test_runner_cleanup(runner);        -- Simulation ends here
  end process;
end memory_processor;
