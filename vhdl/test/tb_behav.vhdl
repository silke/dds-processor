  library vunit_lib;
  use vunit_lib.lang.all;
  use vunit_lib.string_ops.all;
  use vunit_lib.dictionary.all;
  use vunit_lib.path.all;
  use vunit_lib.log_types_pkg.all;
  use vunit_lib.log_special_types_pkg.all;
  use vunit_lib.log_pkg.all;
  use vunit_lib.check_types_pkg.all;
  use vunit_lib.check_special_types_pkg.all;
  use vunit_lib.check_pkg.all;
  use vunit_lib.run_types_pkg.all;
  use vunit_lib.run_special_types_pkg.all;
  use vunit_lib.run_base_pkg.all;
  use vunit_lib.run_pkg.all;
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.processor_types.all;

entity tb_behaviour is
  generic (runner_cfg : string := "");
end tb_behaviour;

architecture memory_processor of tb_behaviour is
  -- for cpu_behav : processor use entity work.processor(behaviour);
  -- for mem_behav : memory use entity work.memory(behaviour);

  component memory
    generic (tpd : time := 1 ns);
    port(d_busout : out std_logic_vector(31 downto 0);
         d_busin  : in  std_logic_vector(31 downto 0);
         a_bus    : in  std_logic_vector(31 downto 0);
         clk      : in  std_ulogic;
         write    : in  std_ulogic;
         read     : in  std_ulogic;
         ready    : out std_ulogic);
  end component;

  component processor
    port (d_busout : out bit32;
          d_busin  : in  bit32;
          a_bus    : out bit32;
          write    : out std_ulogic;
          read     : out std_ulogic;
          ready    : in  std_ulogic;
          reset    : in  std_ulogic;
          clk      : in  std_ulogic);
  end component;

  signal behav_data_from_cpu, behav_addr      : bit32;
  signal behav_data_to_cpu                    : std_logic_vector(31 downto 0);
  signal behav_read, behav_write, behav_ready : std_ulogic;
  signal reset                                : std_ulogic := '0';
  signal clk                                  : std_ulogic := '0';

begin
  cpu_behav : processor
    port map(behav_data_from_cpu,
             bit32(behav_data_to_cpu),
             behav_addr,
             behav_write,
             behav_read,
             behav_ready,
             reset,
             clk);

  mem_behav : memory
    generic map (1 ns)
    port map (behav_data_to_cpu,
              std_logic_vector(behav_data_from_cpu),
              std_logic_vector(behav_addr),
              clk,
              behav_write,
              behav_read,
              behav_ready);

  reset <= '0', '1' after 100 ns;
  clk   <= not clk  after 10 ns;
  main : process
  begin
    test_runner_setup(runner, runner_cfg);
    wait until behav_write = '1';
    check(to_integer(unsigned(behav_data_from_cpu)) = 368, "Invalid behavioural answer writen to memory", failure);
    test_runner_cleanup(runner);        -- Simulation ends here
  end process;
end memory_processor;
