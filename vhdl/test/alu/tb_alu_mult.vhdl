library vunit_lib;
use vunit_lib.lang.all;
use vunit_lib.string_ops.all;
use vunit_lib.dictionary.all;
use vunit_lib.path.all;
use vunit_lib.log_types_pkg.all;
use vunit_lib.log_special_types_pkg.all;
use vunit_lib.log_pkg.all;
use vunit_lib.check_types_pkg.all;
use vunit_lib.check_special_types_pkg.all;
use vunit_lib.check_pkg.all;
use vunit_lib.run_types_pkg.all;
use vunit_lib.run_special_types_pkg.all;
use vunit_lib.run_base_pkg.all;
use vunit_lib.run_pkg.all;
library osvvm;
library ieee;

use osvvm.RandomPkg.all;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.processor_types.all;
use work.multiplier.all;

entity tb_alu_mult is
  generic (runner_cfg : string := "");
end tb_alu_mult;

architecture tb of tb_alu_mult is
  --for t_alu : alu use entity work.alu(implementation);

  component alu
    port(clk   : in  std_ulogic;
         rst   : in  std_ulogic;
         op    : in  op_alu_t;
         op_en : in  bit1;
         rdy   : out bit1;
         r1    : in  int32;
         r2    : in  int32;
         w     : out int32
         );
  end component;

  signal alu_op                : op_alu_t;
  signal alu_op_en, alu_rdy    : bit1       := '0';
  signal alu_r1, alu_r2, alu_w : int32      := (others => '0');
  signal reset                 : std_ulogic := '0';
  signal clk                   : std_ulogic := '0';

begin
  t_alu : alu
    port map (clk, reset, alu_op, alu_op_en, alu_rdy, alu_r1, alu_r2, alu_w);

  reset <= '0', '1' after 50 ns;
  clk   <= not clk  after 10 ns;

  main : process
    procedure enable is
    begin
      wait until clk = '1';
      alu_op_en <= '1';
      wait until clk = '1';
      alu_op_en <= '0';
      wait until alu_rdy = '1';
    end enable;

    variable RV1, RV2 : RandomPType;
    variable temp     : int32;
    variable hi, lo   : integer;

    constant mult_max   : integer := 1431655765;
    constant mult_min   : integer := -1431655766;
  begin
    -- Setup simulation
    test_runner_setup(runner, runner_cfg);
    wait for 100 ns;
    wait until clk = '1';

    -- Generate initial seeds
    RV1.InitSeed (RV1'instance_name);
    RV2.InitSeed (RV2'instance_name);

    -- Test with random values
    for i in 0 to int16_max loop
      -- Generate random values
      alu_r1 <= RV1.RandSigned(mult_min, mult_max, int32'length);
      alu_r2 <= RV2.RandSigned(mult_min, mult_max, int32'length);

      -- Test multiplication
      alu_op <= mult_c;

      -- Calculate mult
      wait until clk = '1';
      algof_mult(to_integer(alu_r1), to_integer(alu_r2), hi, lo);
      enable;

      -- Load and check HI
      alu_op <= mfhi_c;
      enable;
      check(to_integer(alu_w) = hi, "Multiplication hi failed: " & integer'image(to_integer(alu_w)) & " expected " & integer'image(hi), failure);
      wait for 50 ns;

      -- Load and check LO
      alu_op <= mflo_c;
      enable;
      check(to_integer(alu_w) = lo, "Multiplication lo failed: " & integer'image(to_integer(alu_w)) & " expected " & integer'image(lo), failure);

      wait for 50 ns;
    end loop;

    -- Simulation ends here
    test_runner_cleanup(runner);
  end process;
end tb;
