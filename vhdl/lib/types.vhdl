library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
package processor_types is
  -- Bit types
  subtype bit64 is std_ulogic_vector (63 downto 0);
  subtype bit32 is std_ulogic_vector (31 downto 0);
  subtype bit16 is std_ulogic_vector (15 downto 0);
  subtype bit8 is std_ulogic_vector (7 downto 0);
  subtype bit6 is std_ulogic_vector (5 downto 0);
  subtype bit5 is std_ulogic_vector (4 downto 0);
  subtype bit4 is std_ulogic_vector (3 downto 0);
  subtype bit3 is std_ulogic_vector (2 downto 0);
  subtype bit2 is std_ulogic_vector (1 downto 0);
  subtype bit1 is std_ulogic;

  -- Integer types
  subtype int64 is signed(63 downto 0);
  subtype int32 is signed(31 downto 0);
  subtype int30 is signed(29 downto 0);
  subtype int16 is signed(15 downto 0);
  subtype int2 is signed(1 downto 0);
  subtype uint5 is unsigned(4 downto 0);
  subtype uint1 is unsigned (0 downto 0);


  -- Min/max values
  constant int32_max  : integer := integer'high;
  constant int32_min  : integer := integer'low;
  constant int16_max  : integer := 32767;
  constant int16_min  : integer := -32768;
  constant uint16_max : integer := 65535;

  -- Helpful values
  constant zeroes32   : int32 := (others => '0');
  constant ones32     : int32 := (others => '1');
  constant one32      : int32 := zeroes32(31 downto 1) & '1';
  alias zeros32       : int32 is zeroes32;
  alias zero32        : int32 is zeroes32;
  constant zeroes2    : int2  := zeroes32(1 downto 0);
  constant dontcare32 : int32 := (others => '-');

  -- Instructions
  -- Opcodes
  constant rtype : bit6 := "000000";
  constant addi  : bit6 := "001000";
  constant ori   : bit6 := "001101";
  constant lui   : bit6 := "001111";
  constant lw    : bit6 := "100011";
  constant sw    : bit6 := "101011";
  constant beq   : bit6 := "000100";
  constant bgez  : bit6 := "000001";

  -- Functions
  constant add  : bit6 := "100000";
  constant orr  : bit6 := "100101";
  constant sbt  : bit6 := "100010";
  constant slt  : bit6 := "101010";
  constant div  : bit6 := "011010";
  constant mult : bit6 := "011000";
  constant mfhi : bit6 := "010000";
  constant mflo : bit6 := "010010";
  constant nop  : bit6 := "000000";

  -- ALU opcodes
  type op_alu_t is (add_c, sub_c, branch_c, or_c, slt_c, div_c, mult_c, mfhi_c, mflo_c, lui_c);
  type op_add_t is (add_c, sub_c, branch_c, slt_c, abs_c);
  type mux_branch_t is (b_none_c, b_eq_c, b_gez_c);

  -- Register
  type registerMap is array(0 to 31) of integer;
  type bitRegMap is array(0 to 31) of bit32;
  type int32RegMap is array(0 to 31) of int32;

  -- Functions
  function int_bit32(i  : integer) return bit32;
  function uint_bit32(i : integer) return bit32;
  function bit32_int(b  : bit32) return integer;
end processor_types;

package body processor_types is
  function int_bit32(i : integer) return bit32 is
  begin
    return bit32(to_signed(i, bit32'length));
  end int_bit32;

  function uint_bit32(i : integer) return bit32 is
  begin
    return bit32(to_unsigned(i, bit32'length));
  end uint_bit32;

  function bit32_int(b : bit32) return integer is
  begin
    return to_integer(signed(b));
  end bit32_int;
end package body;
