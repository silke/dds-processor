library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package logger is
  procedure log(msg           : in string);
  function std_to_string(vctr : in std_ulogic_vector) return string;
end logger;

package body logger is
  procedure log(msg : in string) is
  begin
    assert false report msg severity note;
  end log;

  function std_to_string(vctr : in std_ulogic_vector) return string is
    variable b    : string (1 to vctr'length) := (others => NUL);
    variable stri : integer                   := 1;
  begin
    for i in vctr'range loop
      b(stri) := std_ulogic'image(vctr((i)))(2);
      stri    := stri+1;
    end loop;
    return b;
  end function;
end logger;
