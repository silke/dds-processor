library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.processor_types.all;
use work.adder.all;
use work.logger.all;

package multiplier is
  procedure behav_mult(s, t : in integer; hi, lo : out integer);
  procedure algof_mult(s, t : in integer; hi, lo : out integer);
  procedure algo1_mult(s, t : in integer; hi, lo : out integer);
end multiplier;

package body multiplier is
  -- Behavioural description
  procedure behav_mult(s, t : in integer; hi, lo : out integer) is
    variable hilo   : bit64;
    variable ss, st : int32;
  begin
    ss := to_signed(s, int32'length);
    st := to_signed(t, int32'length);

    hilo := bit64(ss * st);
    hi   := bit32_int(hilo(63 downto 32));
    lo   := bit32_int(hilo(31 downto 0));
  end behav_mult;

  -- Chosen algorithm
  procedure algof_mult(s, t : in integer; hi, lo : out integer) is
  begin
    algo1_mult(s, t, hi, lo);
  end algof_mult;

  -- Algorithm using shift and add
  procedure algo1_mult(s, t : in integer; hi, lo : out integer) is
    constant zero32 : bit32 := (others => '0');
    variable inter  : bit64;
    variable bs, bt : bit32;
    variable ii     : int32;
    variable sig    : bit1;
    alias inter_hi  : bit32 is inter(63 downto 32);
    alias inter_lo  : bit32 is inter(31 downto 0);
  begin
    -- Initialise required variables
    bs    := int_bit32(s);
    bt    := int_bit32(t);
    inter := zero32 & bs;

    -- Predict sign bit to extend with
    sig := bs(31) xor bt(31);

    -- Loop
    for I in 1 to bit32'length loop
      -- Add when lsb is 1
      if inter(0) = '1' then
        ii       := int32(inter_hi);
        inter_hi := bit32(ii + t);
      end if;

      -- Shift
      inter := sig & inter(63 downto 1);
    end loop;

    -- Save the upper and lower half
    hi := bit32_int(inter_hi);
    lo := bit32_int(inter_lo);
  end algo1_mult;
end multiplier;
