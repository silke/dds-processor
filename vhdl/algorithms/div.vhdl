-- requires: lib/types algorithms/add
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.processor_types.all;
use work.adder.all;
use work.logger.all;
library vunit_lib;
use vunit_lib.log_types_pkg.all;
use vunit_lib.log_special_types_pkg.all;
use vunit_lib.log_pkg.all;

package divider is
  procedure behav_div(s, t : in integer; hi, lo : out integer);
  procedure algof_div(s, t : in integer; hi, lo : out integer);
  procedure algo1_div(s, t : in integer; hi, lo : out integer);
  procedure algo2_div(s, t : in integer; hi, lo : out integer);
  procedure algo3_div(s, t : in integer; hi, lo : out integer);
end divider;

package body divider is
  -- Behavioural description
  procedure behav_div(s, t : in integer; hi, lo : out integer) is
  begin
    if t = 0 then
      hi := 0;
      lo := 0;
    else
      lo := s / t;
      hi := s rem t;
    end if;
  end behav_div;

  -- Chosen algorithm
  procedure algof_div(s, t : in integer; hi, lo : out integer) is
  begin
    algo2_div(s, t, hi, lo);
  end algof_div;

  -- Algorithm using shift and subtract
  procedure algo1_div(s, t : in integer; hi, lo : out integer) is
    variable intermediate : bit64;
    variable bs           : bit32;
    variable bt           : bit32;
    variable diff_sign    : std_logic;
    variable expt_sign    : std_logic;
    variable diff         : integer;
    variable bdiff        : bit32;
    alias inter_hi        : bit32 is intermediate(63 downto 32);
    alias inter_lo        : bit32 is intermediate(31 downto 0);
  begin
    bs := bit32(to_signed(s, bit32'length));
    bt := bit32(to_signed(t, bit32'length));
    -- Initialize intermediate with sign extended alu_s
    intermediate := (31 downto 0 => bs(31)) & bs;
    -- Loop through the bits
    for I in 1 to bit32'length loop
      -- check sign of the partial
      if inter_hi(31) = '0' then
        -- partial * 2 - t
        diff := to_integer(signed(intermediate(62 downto 31))) - t;
      else
        -- partial * 2 + t
        diff := to_integer(signed(intermediate(62 downto 31))) + t;
      end if;
      -- binary diff
      bdiff := bit32(to_signed(diff, bit32'length));
      -- apply new result to the intermediate with 1 append if inter_hi
      intermediate := bdiff & intermediate(30 downto 0) & not inter_hi(31);
    end loop;
    inter_lo := bit32(to_signed( to_integer(signed(inter_lo)) - to_integer(signed( not inter_lo)), bit32'length));
    -- Rem is negative
    if inter_hi(31) = '1' then
      -- Fix quotient
      if s > 0 then
        inter_hi := bit32(to_signed( to_integer(signed(inter_hi)) + abs(t), bit32'length));
      end if;
      inter_lo := bit32(to_signed( to_integer(signed(inter_lo)) - 1, bit32'length));
    end if;
    -- Compensate for rounding
    if (inter_lo(31) = '1') and (to_integer(signed(inter_hi)) /= 0) then
      inter_lo := bit32(to_signed( to_integer(signed(inter_lo)) + 1, bit32'length));
    end if;
    hi := bit32_int(inter_hi);
    lo := bit32_int(inter_lo);
  end algo1_div;

  -- Algorithm using shift and subtract restoring
  procedure algo2_div(s, t : in integer; hi, lo : out integer) is
    variable intermediate : bit64;
    variable bs           : bit32;
    variable bt           : bit32;
    variable signed_s, signed_t : signed(31 downto 0);
    variable diff_sign    : std_logic;
    variable expt_sign    : std_logic;
    variable diff,abst, abss         : signed(31 downto 0);
    variable bdiff        : bit32;
    alias inter_hi        : bit32 is intermediate(63 downto 32);
    alias inter_lo        : bit32 is intermediate(31 downto 0);
  begin
    abss := to_signed(abs(s), bit32'length);
    abst := to_signed(abs(t), bit32'length);
    bs := bit32(abss);
    bt := bit32(abst);
    signed_s := to_signed(s, bit32'length);
    signed_t := to_signed(t, bit32'length);
    intermediate := (31 downto 0 => '0') & bs;
    for I in 1 to bit32'length loop
      diff := signed(intermediate(62 downto 31)) - abst;
      bdiff := bit32(diff);
      if bdiff(31) = '1' then
        intermediate := intermediate(62 downto 0) & '0';
      else
        intermediate := bdiff & intermediate(30 downto 0) & '1';
      end if;
    end loop;
    if (signed_s(31) xor signed_t(31)) = '1' then
      inter_lo := bit32((not signed(inter_lo)) + 1 );
    end if;
    if s < 0 then
      inter_hi := bit32((not signed(inter_hi)) + 1 );
    end if;
    hi := bit32_int(inter_hi);
    lo := bit32_int(inter_lo);
  end algo2_div;
  
  -- Algorithm using shift and subtract restoring
  procedure algo3_div(s, t : in integer; hi, lo : out integer) is
    variable intermediate : bit64;
    variable bs           : bit32;
    variable bt           : bit32;
    variable signed_s, signed_t : signed(31 downto 0);
    variable diff_sign    : std_logic;
    variable expt_sign    : std_logic;
    variable compensation : std_logic;
    variable diff,abst, abss         : signed(31 downto 0);
    variable bdiff        : bit32;
    alias inter_hi        : bit32 is intermediate(63 downto 32);
    alias inter_lo        : bit32 is intermediate(31 downto 0);
  begin
    abss := to_signed(abs(s), bit32'length);
    abst := to_signed(abs(t), bit32'length);
    bs := bit32(abss);
    bt := bit32(abst);
    signed_s := to_signed(s, bit32'length);
    signed_t := to_signed(t, bit32'length);
    diff := abss - abst;
    if ((signed_s(31) xor signed_t(31)) = '1') and (diff(31) = '0') then
      compensation := '1';
      if signed_s(31) = '0' then
          intermediate := (31 downto 0 => '0') & bit32(diff);
      else
          intermediate := (31 downto 0 => '0') & bit32(diff - 1);
      end if;
    else
      compensation := '0';
      if signed_s(31) = '0' then
          intermediate := (31 downto 0 => '0') & bit32(abss);
      else
          intermediate := (31 downto 0 => '0') & bit32(abss -  1);
      end if;
    end if;
    for I in 1 to bit32'length loop
      diff := signed(intermediate(62 downto 31)) - abst;
      bdiff := bit32(diff);
      if bdiff(31) = '1' then
        intermediate := intermediate(62 downto 0) & '0';
      else
        intermediate := bdiff & intermediate(30 downto 0) & '1';
      end if;
    end loop;
    if compensation = '1' then
      inter_lo := bit32(not inter_lo);
    end if;
    if signed_s(31) = '1' then
      inter_hi := bit32(not inter_hi);
    end if;
    hi := bit32_int(inter_hi);
    lo := bit32_int(inter_lo);
  end algo3_div;
end divider;
