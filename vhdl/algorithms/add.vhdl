library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package adder is
  function behav_add(s, t : integer) return integer;
  function algof_add(s, t : integer) return integer;
end adder;

package body adder is
  -- Behavioural description
  function behav_add(s, t : integer) return integer is
  begin
    return s + t;
  end behav_add;

  -- Algorithm using dedicated adder
  -- This is the same as the behavioural description
  function algof_add(s, t : integer) return integer is
  begin
    return s + t;
  end algof_add;
end adder;
