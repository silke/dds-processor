library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.processor_types.all;

package load_upper_i is
  function behav_lui(i : bit16) return integer;
  function algof_lui(i : bit16) return integer;
end load_upper_i;

package body load_upper_i is
  -- Behavioural description
  function behav_lui(i : bit16) return integer is
  begin
    return to_integer(signed(i)) * 2**16;
  end behav_lui;

  -- Algorithm using simple bit mapping
  function algof_lui(i : bit16) return integer is
    variable result   : bit32;
    constant zeroes16 : bit16 := (others => '0');
  begin
    result := i & zeroes16;

    return to_integer(signed(result));
  end algof_lui;
end load_upper_i;
