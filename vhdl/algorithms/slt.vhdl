library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.processor_types.all;
use work.logger.all;
use work.subtractor.all;

package less_than is
  function behav_slt(s, t : integer) return integer;
  function algof_slt(s, t : integer) return integer;
end less_than;

package body less_than is
  -- Behavioural description
  function behav_slt(s, t : integer) return integer is
  begin
    if s < t then
      return 1;
    else
      return 0;
    end if;
  end behav_slt;

  -- Algorithm
  function algof_slt(s, t : integer) return integer is
    variable temp   : integer;
    variable bitval : bit32;
    alias sig       : bit1 is bitval(31);
  begin
    temp   := s - t;
    bitval := int_bit32(temp);

    if sig = '1' then
      return 1;
    else
      return 0;
    end if;
  end algof_slt;
end less_than;
