library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.processor_types.all;
use work.subtractor.all;

package branch_eq is
  function behav_beq(s, t, i, pc : integer) return integer;
  function algof_beq(s, t, i, pc : integer) return integer;
  function algo1_beq(s, t, i, pc : integer) return integer;
  function algo2_beq(s, t, i, pc : integer) return integer;
end branch_eq;

package body branch_eq is
  -- Behavioural description
  function behav_beq(s, t, i, pc : integer) return integer is
  begin
    if s = t then
      return pc + (i * 4);
    else
      return pc;
    end if;
  end behav_beq;

  -- Chosen algorithm
  function algof_beq(s, t, i, pc : integer) return integer is
  begin
    return algo1_beq(s, t, i, pc);
  end algof_beq;

  -- Algoritm using the difference to check equality
  -- It is assumed that *4 is the same as a left shift by 2
  function algo1_beq(s, t, i, pc : integer) return integer is
  begin
    if algof_sub(s, t) = 0 then
      return pc + (i * 4);
    end if;
    return pc;
  end algo1_beq;

  -- Algoritm using an and to see if they are the same
  -- It is assumed that *4 is the same as a left shift by 2
  function algo2_beq(s, t, i, pc : integer) return integer is
    variable bs,bt : bit32;
    variable x : integer;
  begin
    bs := int_bit32(s);
    bt := int_bit32(t);

    if bs = bt then
      return pc + (i * 4);
    end if;

    return pc;
  end algo2_beq;
end branch_eq;
