library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.processor_types.all;

package branch_gez is
  function behav_bgez(s, i, pc : integer) return integer;
  function algof_bgez(s, i, pc : integer) return integer;
end branch_gez;

package body branch_gez is
  -- Behavioural description
  function behav_bgez(s, i, pc : integer) return integer is
  begin
    if s >= 0 then
      return pc + (i * 4);
    else
      return pc;
    end if;
  end behav_bgez;

  -- Algorithm checking the sign bit for a negative number
  -- It is assumed that *4 is the same as a left shift by 2
  function algof_bgez(s, i, pc : integer) return integer is
    variable bitval : bit32;
    alias sig       : bit1 is bitval(31);
  begin
    bitval := int_bit32(s);

    if sig = '0' then
      return pc + (i * 4);
    end if;

    return pc;
  end algof_bgez;
end branch_gez;

-- Microcode
-- bgez rs,i
