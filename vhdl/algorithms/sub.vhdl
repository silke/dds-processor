library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.processor_types.all;
use work.adder.all;

package subtractor is
  function behav_sub(s, t : integer) return integer;
  function algof_sub(s, t : integer) return integer;
  function algo1_sub(s, t : integer) return integer;
  function algo2_sub(s, t : integer) return integer;
end subtractor;

package body subtractor is
  -- Behavioural description
  function behav_sub(s, t : integer) return integer is
  begin
    return s - t;
  end behav_sub;

  -- Chosen algorithm
  function algof_sub(s, t : integer) return integer is
  begin
    return algo2_sub(s, t);
  end algof_sub;

  -- Algorithm using dedicated subtractor
  -- This is the same as the behavioural description
  function algo1_sub(s, t : integer) return integer is
  begin
    return s - t;
  end algo1_sub;

  -- Using XOR and ADD
  function algo2_sub(s, t : integer) return integer is
    variable bs, bt : bit32;
    variable tt     : integer;
    constant ones   : bit32 := (others => '1');
  begin
    bs := bit32(to_signed(s, bit32'length));
    bt := bit32(to_signed(t, bit32'length));

    bt := bt xor ones;
    tt := to_integer(signed(bt));
    return algof_add(s, algof_add(tt, 1));
  end algo2_sub;
end subtractor;
