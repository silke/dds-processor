library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.processor_types.all;

package orrer is
  function behav_or(s, t : integer) return integer;
  function algof_or(s, t : integer) return integer;
end orrer;

package body orrer is
  -- Behavioural description
  function behav_or(s, t : integer) return integer is
    variable bs, bt : bit32;
  begin
    bs := bit32(to_signed(s, bit32'length));
    bt := bit32(to_signed(t, bit32'length));

    return to_integer(signed(bs or bt));
  end behav_or;

  -- Algorithmic description is the same as the behavioural
  function algof_or(s, t : integer) return integer is
  begin
    return behav_or(s, t);
  end algof_or;
end orrer;
