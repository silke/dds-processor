-- The entity memory contains conversion functions used in the automatically
-- generated architecture for the memeory.
-- Declaring it locally prevents from using it elsewehere in a design unit.
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.processor_types.all;
entity memory is
  generic (tpd : time := 1 ns);
  port(d_busout : out bit16;
       d_busin  : in  bit16;
       a_bus    : in  bit16;
       write    : in  std_ulogic;
       read     : in  std_ulogic;
       ready    : out std_ulogic);

  procedure int2bitv(int : in integer; bitv : out std_ulogic_vector) is
  begin
    bitv := std_ulogic_vector(to_signed(int, bitv'length));
  end int2bitv;

  function bitv2int(bitv : in std_ulogic_vector) return integer is
  begin
    return to_integer(signed(bitv));
  end bitv2int;

  function bitv2nat (bitv : in std_ulogic_vector) return natural is
  begin
    return to_integer(unsigned(bitv));
  end bitv2nat;
end memory;
--------------------------------------------------------
architecture behaviour of memory is
begin
  process
    constant low_address  : natural := 0;
    constant high_address : natural := 300;  -- upper limit of the memory
                                        -- INCREASE this number IF the program
                                        -- needs more memory. Don't FORget
                                        -- that the addresses used to write
                -- to and read from should be available.
    type     memory_array is
      array (natural range low_address to high_address) of integer;
    variable mem : memory_array :=
      (18,      --        mov #6,d0        0000 0000 0001 0010
       6,       --                         0000 0000 0000 0110
       20,      --        mov #62,a0       0000 0000 0001 0100
       62,      --                         0000 0000 0011 1110
       21,      --        mov #63,a1       0000 0000 0001 0101
       63,      --                         0000 0000 0011 1111
       19,      --        mov #1,d1        0000 0000 0001 0011
       1,       --                         0000 0000 0000 0001
       54,      --        mov d1,(a0)      0000 0000 0011 0110
       55,      --        mov d1,(a1)      0000 0000 0011 0111
       13347,   -- lbl:   comp d0,d1       0011 0100 0010 0011
       -31999,  --        bvs einde:       1000 0011 0000 0001
       9,       --                         0000 0000 0000 1001
       9235,    --        add #1,d1        0010 0100 0001 0011
       1,       --                         0000 0000 0000 0001
       55,      --        mov d1,(a1)      0000 0000 0011 0111
       11875,   --        mul (a0),d1      0010 1110 0110 0011
       -3836,   --        deca a0          1111 0001 0000 0100
       54,      --        mov d1,(a0)      0000 0000 0011 0110
       115,     --        mov (a1),d1      0000 0000 0111 0011
       -32767,  --        bra lbl:         1000 0000 0000 0001
       -12,     --                         1111 1111 1111 0100
       -32767,  -- einde: bra einde:       1000 0000 0000 0001
       -2,      --                         1111 1111 1111 1110
       others => 0
       );
    variable address  : natural;
    variable data_out : bit16;
    constant unknown  : bit16 := (others => '-');
  begin
    ready                                        <= '0' after tpd;
    --
    -- WAIT FOR a command
    --
    wait until (read = '1') or (write = '1');
    address                                      := bitv2nat(a_bus);
    assert (address >= low_address) and (address <= high_address)
      report "out of memory range" severity warning;
    if write = '1'
    then
      mem(address) := bitv2int(d_busin);
      ready        <= '1' after tpd;
      wait until write = '0';           -- WAIT UNTIL END of write cycle
    else                                -- read ='1';
      int2bitv(mem(address), data_out);
      d_busout <= data_out;
      ready    <= '1' after tpd;
      wait until read = '0';
      d_busout <= unknown;
    end if;
  end process;
end behaviour;
