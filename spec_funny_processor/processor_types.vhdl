library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
package processor_types is
  subtype bit16 is std_ulogic_vector (15 downto 0);
  subtype bit8 is std_ulogic_vector (7 downto 0);
  subtype bit4 is std_ulogic_vector (3 downto 0);
  subtype bit3 is std_ulogic_vector (2 downto 0);

  -- instruction set
  constant mov : bit8 := "00000000";

  constant subt    : bit8 := "00100000";
  constant abssub  : bit8 := "00100001";
  constant absmsub : bit8 := "00100010";

  constant add     : bit8 := "00100100";
  constant absadd  : bit8 := "00100101";
  constant absmadd : bit8 := "00100110";

  constant maxi : bit8 := "00101000";
  constant maxa : bit8 := "00101001";
  constant mini : bit8 := "00101010";
  constant mina : bit8 := "00101011";

  constant absl   : bit8 := "00101100";
  constant absmin : bit8 := "00101101";

  constant mul    : bit8 := "00101110";
  constant absmul : bit8 := "00101111";

  constant kl   : bit8 := "00110000";
  constant klg  : bit8 := "00110001";
  constant kla  : bit8 := "00110010";
  constant klga : bit8 := "00110011";
  constant comp : bit8 := "00110100";

  constant asl    : bit8 := "01000000";
  constant asr    : bit8 := "01000001";
  constant lsl    : bit8 := "01000010";
  constant lsr    : bit8 := "01000011";
  constant rol_87 : bit8 := "01000100";
  constant ror_87 : bit8 := "01000101";

  constant bra : bit8 := "10000000";
  constant beq : bit8 := "10000001";
  constant bne : bit8 := "10000010";
  constant bvs : bit8 := "10000011";
  constant bvc : bit8 := "10000100";
  constant bpl : bit8 := "10000101";
  constant bmi : bit8 := "10000110";

  constant nset : bit8 := "11100000";
  constant nclr : bit8 := "11100001";
  constant zset : bit8 := "11100010";
  constant zclr : bit8 := "11100011";
  constant vset : bit8 := "11100100";
  constant vclr : bit8 := "11100101";

  constant inca : bit8 := "11110000";
  constant deca : bit8 := "11110001";

-- source and destination
  constant none   : bit4 := "0000";
  constant imm    : bit4 := "0001";
  constant rd0    : bit4 := "0010";
  constant rd1    : bit4 := "0011";
  constant ra0    : bit4 := "0100";
  constant ra1    : bit4 := "0101";
  constant a0_ind : bit4 := "0110";
  constant a1_ind : bit4 := "0111";


-- sets the conditonal code register bits and rd.
  procedure set_cc_rd (data : in  integer;
                       cc   : out bit3;
                       rd   : out bit16);
-- sets the conditonal code register bits and rd.
  type     bool2std_ulogic_table is array (boolean) of std_ulogic;
  constant bool2std : bool2std_ulogic_table := (false => '0', true => '1');
  type     direction is (left, right);
  type     domain is (logical, arithmetic);
  function shift(x  : std_ulogic_vector; dir : direction; mode : domain)
    return std_ulogic_vector;
  function rotate(x : std_ulogic_vector; dir : direction)
    return std_ulogic_vector;
  function member(x : std_ulogic_vector; list : std_ulogic_vector)
    return boolean;
  -- is x member of the list, where x is a std_ulogic_vector
  -- and list is a concentatenation of these std_ulogic_vectors
  -- exa. x=001 and list=000_100_011, hence x is not in the list

end processor_types;

package body processor_types is
  procedure set_cc_rd (data : in  integer;
                       cc   : out bit3;
                       rd   : out bit16) is
    alias cc_n    : std_ulogic is cc(2);
    alias cc_z    : std_ulogic is cc(1);
    alias cc_v    : std_ulogic is cc(0);
    constant low  : integer := -2**15;
    constant high : integer := 2**15-1;
  begin
    if (data < low) or (data > high)
    then                                -- overflow
      assert false report "overflow situation in arithmetic operation" severity
        note;
      cc_v := '1'; cc_n := '-'; cc_z := '-'; rd := (others => '-');
    else
      cc_v := '0'; cc_n := bool2std(data < 0); cc_z := bool2std(data = 0);
      rd   := std_ulogic_vector(to_signed(data, 16));
    end if;
  end set_cc_rd;

  function shift(x : std_ulogic_vector; dir : direction; mode : domain)
    return std_ulogic_vector is
    variable tmp : std_ulogic_vector(x'length downto 1) := x;
  begin
    case dir is
      when left  => return tmp(tmp'length-1 downto 1) & '0';
      when right =>
        case mode is
          when logical    => return '0' & tmp(tmp'length downto 2);
          when arithmetic => return tmp(tmp'length) & tmp(tmp'length downto 2);
        end case;
    end case;
  end shift;

  function rotate(x : std_ulogic_vector; dir : direction)
    return std_ulogic_vector is
    variable tmp : std_ulogic_vector(x'length downto 1) := x;
  begin
    case dir is
      when left  => return tmp(tmp'length-1 downto 1) & tmp(tmp'length);
      when right => return tmp(1) & tmp(tmp'length downto 2);
    end case;
  end rotate;

  function member(x : std_ulogic_vector; list : std_ulogic_vector) return boolean is
    variable lgt_x    : natural                               := x'length;
    variable lgt_list : natural                               := list'length;
    variable llist    : std_ulogic_vector(0 to list'length-1) := list;
    variable i        : natural                               := 0;
  begin
    while i < lgt_list loop
      if x = llist(i to i+lgt_x-1) then return true; end if;
      i := i+lgt_x;
    end loop;
    return false;
  end member;

end processor_types;
