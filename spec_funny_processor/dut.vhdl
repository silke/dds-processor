entity dut is
end dut;

library ieee;
use ieee.std_logic_1164.all;
use work.processor_types.all;
architecture memory_processor of dut is
  component memory
    generic (tpd : time := 1 ns);
    port(d_busout : out bit16;
         d_busin  : in  bit16;
         a_bus    : in  bit16;
         write    : in  std_ulogic;
         read     : in  std_ulogic;
         ready    : out std_ulogic);
  end component;
  component processor
    port (d_busout : out bit16;
          d_busin  : in  bit16;
          a_bus    : out bit16;
          write    : out std_ulogic;
          read     : out std_ulogic;
          ready    : in  std_ulogic;
          reset    : in  std_ulogic;
          clk      : in  std_ulogic);
  end component;
  signal data_from_cpu, data_to_cpu, addr : bit16;
  signal read, write, ready               : std_ulogic;
  signal reset                            : std_ulogic := '1';
  signal clk                              : std_ulogic := '0';
begin
  cpu : processor
    port map(data_from_cpu, data_to_cpu, addr, write, read, ready, reset, clk);
  mem : memory
    generic map (1 ns)
    port map (data_to_cpu, data_from_cpu, addr, write, read, ready);
  reset <= '1', '0' after 100 ns;
  clk   <= not clk  after 10 ns;
end memory_processor;
--------------------------------------------------------
configuration test_of_mem_proc of dut is
  for memory_processor
    for cpu : processor use entity work.processor (behaviour); end for;
    for mem : memory use entity work.memory (behaviour); end for;
  end for;
end test_of_mem_proc;
