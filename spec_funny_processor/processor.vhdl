library ieee;
use ieee.std_logic_1164.all;
use work.processor_types.all;
entity processor is
  port (d_busout : out bit16;
        d_busin  : in  bit16;
        a_bus    : out bit16;
        write    : out std_ulogic;
        read     : out std_ulogic;
        ready    : in  std_ulogic;
        reset    : in  std_ulogic;
        clk      : in  std_ulogic);
end processor;

library ieee;
use ieee.numeric_std.all;
architecture behaviour of processor is
begin
  process
    variable pc             : natural;
    variable a0             : bit16;
    variable a1             : bit16;
    variable d0             : bit16;
    variable d1             : bit16;
    variable cc             : bit3;
    alias cc_n              : std_ulogic is cc(2);
    alias cc_z              : std_ulogic is cc(1);
    alias cc_v              : std_ulogic is cc(0);
    variable data           : bit16;
    variable current_instr  : bit16;
    alias op                : bit8 is current_instr(15 downto 8);
    alias src               : bit4 is current_instr(7 downto 4);
    alias dst               : bit4 is current_instr(3 downto 0);
    variable error_src_dst  : boolean;  -- error in src or dst in insruction
    variable rs, rd         : bit16;    -- temporary variables
    variable rs_int, rd_int : integer;  -- integer representation of rs, rd.
    variable rs_low, rd_low : integer;  --    "        "  positions 7..0 of rs and rd.
    variable rc             : std_ulogic;  --    "
    variable displacement   : bit16;
    variable jump           : boolean;  -- used in branch instructions
    variable tmp            : bit16;
    constant one            : bit16 := (0      => '1', others => '0');
    constant dontcare       : bit16 := (others => '-');

    procedure memory_read (addr   : in  natural;
                           result : out bit16) is
      -- Used 'global' signals are:
      --   clk, reset, ready, read, a_bus, d_busin
      -- read data from addr in memory
    begin
      -- put address on output
      a_bus <= std_ulogic_vector(to_unsigned(addr, 16));
      wait until clk = '1';
      if reset = '1' then
        return;
      end if;

      loop                              -- ready must be low (handshake)
        if reset = '1' then
          return;
        end if;
        exit when ready = '0';
        wait until clk = '1';
      end loop;

      read <= '1';
      wait until clk = '1';
      if reset = '1' then
        return;
      end if;

      loop
        wait until clk = '1';
        if reset = '1' then
          return;
        end if;

        if ready = '1' then
          result := d_busin;
          exit;
        end if;
      end loop;
      wait until clk = '1';
      if reset = '1' then
        return;
      end if;

      read  <= '0';
      a_bus <= dontcare;
    end memory_read;

    procedure memory_write(addr : in natural;
                           data : in bit16) is
      -- Used 'global' signals are:
      --   clk, reset, ready, write, a_bus, d_busout
      -- write data to addr in memory
      variable add : bit16;
    begin
      -- put address on output
      a_bus <= std_ulogic_vector(to_unsigned(addr, 16));
      wait until clk = '1';
      if reset = '1' then
        return;
      end if;

      loop                              -- ready must be low (handshake)
        if reset = '1' then
          return;
        end if;
        exit when ready = '0';
        wait until clk = '1';
      end loop;

      d_busout <= data;
      wait until clk = '1';
      if reset = '1' then
        return;
      end if;
      write <= '1';

      loop
        wait until clk = '1';
        if reset = '1' then
          return;
        end if;
        exit when ready = '1';
      end loop;
      wait until clk = '1';
      if reset = '1' then
        return;
      end if;
      --
      write    <= '0';
      d_busout <= dontcare;
      a_bus    <= dontcare;
    end memory_write;

    procedure read_data(s_d    : in    bit4;
                        d0, d1 : in    bit16;
                        a0, a1 : in    bit16;
                        pc     : inout natural;
                        data   : out   bit16) is
      -- read data from d0,d1,a0,a1,(a0),(a1),imm
      variable tmp : bit16;
    begin
      case s_d is
        when rd0    => data := d0;
        when rd1    => data := d1;
        when ra0    => data := a0;
        when ra1    => data := a1;
        when a0_ind => memory_read(to_integer(unsigned(a0)), data);
        when a1_ind => memory_read(to_integer(unsigned(a1)), data);
        when imm    => memory_read(pc, data);
                       pc := pc + 1;
        when others => assert false report "illegal src/dst while reading data"
                         severity warning;
      end case;
    end read_data;

    procedure write_data(s_d    : in    bit4;
                         d0, d1 : inout bit16;
                         a0, a1 : inout bit16;
                         pc     : inout natural;
                         data   : in    bit16) is
      -- write data to d0,d1,a0,a1,(a0),(a1),imm
      variable tmp  : bit16;
      variable addr : bit16;
    begin
      case s_d is
        when rd0    => d0 := data;
        when rd1    => d1 := data;
        when ra0    => a0 := data;
        when ra1    => a1 := data;
        when a0_ind => memory_write(to_integer(unsigned(a0)), data);
        when a1_ind => memory_write(to_integer(unsigned(a1)), data);
        when imm    => memory_read(pc, addr);
                       pc := pc + 1;
                       memory_write(to_integer(unsigned(addr)), data);
        when others => assert false report "illegal src or dst while writing data"
                         severity warning;
      end case;
    end write_data;

  begin
    --
    -- check FOR reset active
    --
    if reset = '1' then
      read  <= '0';
      write <= '0';
      pc    := 0;
      cc    := "000";                   -- clear condition code register
      loop                              -- synchrone reset
        wait until clk = '1';
        exit when reset = '0';
      end loop;
    end if;
    --
    -- fetch next instruction
    --
    memory_read(pc, current_instr);
    if reset /= '1' then
      pc := pc+1;
      --
      -- decode & execute
      --
      case op is

        when mov =>
          error_src_dst := not member(src, rd0&rd1&ra0&ra1&a0_ind&a1_ind&imm) or
                          not member(dst, rd0&rd1&ra0&ra1&a0_ind&a1_ind&imm) or
                          ((src = imm) and (dst = imm));
          assert not error_src_dst report "illegal inst. mov"
            severity warning;
          read_data(src, d0, d1, a0, a1, pc, rs);
          write_data(dst, d0, d1, a0, a1, pc, rs);
          cc := cc;  -- condition code register is unchanged.

        when subt|abssub|absmsub|add|absadd|absmadd|maxi|maxa|mini|mina|
          absl|absmin|mul|absmul =>
          error_src_dst := not member(src, rd0&rd1&a0_ind&a1_ind&imm) or
                          not member(dst, rd0&rd1);
          assert not error_src_dst report "illegal inst. ARITHMETIC" severity warning;
          read_data(src, d0, d1, a0, a1, pc, rs); rs_int := to_integer(signed(rs));
          rs_low                                         := to_integer(signed(rs(7 downto 0)));
          read_data(dst, d0, d1, a0, a1, pc, rd); rd_int := to_integer(signed(rd));
          rd_low                                         := to_integer(signed(rd(7 downto 0)));
          case op is
            when subt    => rd_int := rd_int - rs_int;
            when abssub  => rd_int := abs(rd_int - rs_int);
            when absmsub => rd_int := -abs(rd_int - rs_int);
            when add     => rd_int := rd_int + rs_int;
            when absadd  => rd_int := abs(rd_int + rs_int);
            when absmadd => rd_int := -abs(rd_int + rs_int);

            when maxi => if rs_int > rd_int
                            then rd_int := rs_int;
                            end if;
            when maxa => if abs(rs_int) > abs(rd_int)
                            then rd_int := abs(rs_int);
                            else rd_int := abs(rd_int);
                            end if;
            when mini => if rs_int < rd_int
                            then rd_int := rs_int;
                            end if;
            when mina => if abs(rs_int) < abs(rd_int)
                            then rd_int := abs(rs_int);
                            else rd_int := abs(rd_int);
                            end if;
            when absl   => rd_int := abs(rs_int);
            when absmin => rd_int := -abs(rs_int);
            when mul    => rd_int := rd_low * rs_low;
            when absmul => rd_int := abs (rd_low * rs_low);
            when others => null;
          end case;
          set_cc_rd(rd_int, cc, rd);
          write_data(dst, d0, d1, a0, a1, pc, rd);

        when kl|klg|kla|klga|comp =>
          error_src_dst := not member(src, rd0&rd1&a0_ind&a1_ind&imm) or
                          not member(dst, rd0&rd1);
          assert not error_src_dst report "illegal inst. COMPARE" severity warning;
          read_data(src, d0, d1, a0, a1, pc, rs); rs_int := to_integer(signed(rs));
          read_data(dst, d0, d1, a0, a1, pc, rd); rd_int := to_integer(signed(rd));
          case op is
            when kl     => cc_v := bool2std(rd_int < rs_int);
            when klg    => cc_v := bool2std(rd_int <= rs_int);
            when kla    => cc_v := bool2std(abs(rd_int) < abs(rs_int));
            when klga   => cc_v := bool2std(abs(rd_int) <= abs(rs_int));
            when comp   => cc_v := bool2std(rd = rs);
            when others => null;
          end case;
          cc_n := '-'; cc_z := '-';

        when asl|asr|lsl|lsr|rol_87|ror_87 =>
          error_src_dst := not member(src, none) or
                          not member(dst, rd0&rd1);
          assert not error_src_dst report "illegal inst. SHIFT" severity warning;
          read_data(dst, d0, d1, a0, a1, pc, rd);
          case op is
            when asl    => rd := shift(rd, left, arithmetic);
            when asr    => rd := shift(rd, right, arithmetic);
            when lsl    => rd := shift(rd, left, logical);
            when lsr    => rd := shift(rd, right, logical);
            when rol_87 => rd := rotate(rd, left);
            when ror_87 => rd := rotate(rd, right);
            when others => null;
          end case;
          cc := "---";
          write_data(dst, d0, d1, a0, a1, pc, rd);

        when bra|beq|bne|bvs|bvc|bpl|bmi =>
          error_src_dst := not member(src, none) or
                          not member(dst, a0_ind&a1_ind&imm);
          assert not error_src_dst report "illegal inst. BRANCH" severity warning;
          case op is
            when bra    => jump := true;
            when beq    => jump := cc_z = '1';
            when bne    => jump := cc_z = '0';
            when bvs    => jump := cc_v = '1';
            when bvc    => jump := cc_v = '0';
            when bpl    => jump := cc_n = '0';
            when bmi    => jump := cc_n = '1';
            when others => null;
          end case;
          -- condition code register has NOT changed
          if jump
          then
            case dst is
              when imm => memory_read(pc, displacement);
                             pc := pc +1;
              when a0_ind => memory_read(to_integer(unsigned(a0)), displacement);
              when a1_ind => memory_read(to_integer(unsigned(a1)), displacement);
              when others => assert false report "illegal destination in BRANCH instruction"
                               severity warning;
            end case;
            pc := pc + to_integer(signed(displacement));
          else if dst = imm then pc := pc + 1; end if;  -- skip contents next address
      end if;

    when nset|nclr|zset|zclr|vset|vclr =>
    error_src_dst := not member(src, none) or
                    not member(dst, none);
    assert not error_src_dst report "illegal instruction SET or CLR of CC" severity warning;
    case op is
      when nset   => cc_n := '1';
      when nclr   => cc_n := '0';
      when zset   => cc_z := '1';
      when zclr   => cc_z := '0';
      when vset   => cc_v := '1';
      when vclr   => cc_v := '0';
      when others => null;
    end case;
    -- other condition code bits will be NOT changed

    when inca|deca =>
    error_src_dst := not member(src, none) or
                    not member(dst, ra0&ra1);
    assert not error_src_dst report "illegal inst. INCA, DECA" severity warning;
    case op is
      when inca =>
        case dst is
          when ra0 => if a0 = (a0'range => '1')  -- upper bound?
                      then a0 := (others => '-');
                      else a0 := std_ulogic_vector(unsigned(a0)+1);
                      end if;
          when ra1 => if a1 = (a1'range => '1')  -- upper bound?
                      then a1 := (others => '-');
                      else a1 := std_ulogic_vector(unsigned(a1)+1);
                      end if;
          when others => null;
        end case;
      when deca =>
        case dst is
          when ra0 => if a0 = (a0'range => '0')  -- lower bound?
                      then a0 := (others => '-');
                      else a0 := std_ulogic_vector(unsigned(a0)-1);
                      end if;
          when ra1 => if a1 = (a1'range => '0')  -- lower bound?
                      then a1 := (others => '-');
                      else a1 := std_ulogic_vector(unsigned(a1)-1);
                      end if;
          when others => null;
        end case;
      when others => null;
    end case;
    cc := "---";

    when others => assert false report "illegal instruction" severity warning;

  end case;
end if;
end process;
end behaviour;
