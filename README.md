# MIPS subset processor
[![build status](https://git.snt.utwente.nl/dds/processor/badges/master/build.svg)](https://git.snt.utwente.nl/dds/processor/commits/master)
[![coverage report](https://git.snt.utwente.nl/dds/processor/badges/master/coverage.svg)](https://git.snt.utwente.nl/dds/processor/commits/master)

An implementation of a subset of the MIPS instructions in VHDL.
Tests are done with [VUnit][] and [GHDL][]

[VUnit]: https://vunit.github.io/
[GHDL]:  http://ghdl.free.fr/
