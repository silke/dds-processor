set PATH ../../vhdl
set LIB ../libraries

vcom $PATH/lib/types.vhdl
vcom $PATH/lib/logger.vhdl
vcom $PATH/algorithms/add.vhdl
vcom $PATH/algorithms/sub.vhdl
vcom $PATH/algorithms/div.vhdl
vcom $PATH/algorithms/mult.vhdl
vcom $PATH/implementation/adder.vhdl
vcom $PATH/implementation/alu.vhdl

vcom $PATH/test/alu/tb_alu_add.vhdl

vsim tb_alu_add
add wave -position end sim:*
run -all
