set PATH ../../vhdl
set LIB ../libraries

vcom $PATH/lib/types.vhdl
vcom $PATH/lib/logger.vhdl
vcom $PATH/opt/conf/memory_config.vhdl
vcom $PATH/opt/lib/memory.vhdl
vcom $PATH/implementation/adder.vhdl
vcom $PATH/implementation/alu.vhdl
vcom $PATH/implementation/decoder.vhdl
vcom $PATH/implementation/memory_controller.vhdl
vcom $PATH/implementation/register.vhdl
vcom $PATH/implementation/muxer.vhdl
vcom $PATH/implementation/synth_implementation.vhdl
vcom $PATH/behaviour/processor.vhdl
vcom $PATH/behaviour/processor_behaviour.vhdl

vcom $PATH/test/tb_implementation.vhdl

vsim tb_implementation
add wave -position end sim:*
run -all
