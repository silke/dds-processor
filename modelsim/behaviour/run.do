set PATH ../../vhdl
set LIB ../libraries

vcom $PATH/lib/types.vhdl
vcom $PATH/lib/logger.vhdl
vcom $PATH/opt/conf/memory_config.vhdl
vcom $PATH/opt/lib/memory.vhdl
vcom $PATH/behaviour/processor.vhdl
vcom $PATH/behaviour/processor_behaviour.vhdl

vcom $PATH/test/tb_behav.vhdl

vsim work.tb_behaviour
add wave -position end sim:*
run -all
