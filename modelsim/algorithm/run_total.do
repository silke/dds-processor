set PATH ../../vhdl
set LIB ../libraries

vcom $PATH/lib/types.vhdl
vcom $PATH/lib/logger.vhdl
vcom $PATH/opt/conf/memory_config.vhdl
vcom $PATH/algorithms/sub.vhdl
vcom $PATH/algorithms/add.vhdl
vcom $PATH/algorithms/slt.vhdl
vcom $PATH/algorithms/lui.vhdl
vcom $PATH/algorithms/mult.vhdl
vcom $PATH/algorithms/div.vhdl
vcom $PATH/algorithms/or.vhdl
vcom $PATH/algorithms/beq.vhdl
vcom $PATH/algorithms/bgez.vhdl
vcom $PATH/opt/lib/memory.vhdl
vcom $PATH/behaviour/processor.vhdl
vcom $PATH/behaviour/processor_behaviour.vhdl
vcom $PATH/behaviour/processor_framework.vhdl

# compile test benches
vcom $PATH/test/tb_algo.vhdl

vsim tb_algo
add wave -position end sim:*
run -all
