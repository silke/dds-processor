set PATH ../../vhdl
set LIB ../libraries

vcom $PATH/lib/types.vhdl
vcom $PATH/lib/logger.vhdl
vcom $PATH/algorithms/sub.vhdl
vcom $PATH/algorithms/add.vhdl
vcom $PATH/algorithms/slt.vhdl
vcom $PATH/algorithms/lui.vhdl
vcom $PATH/algorithms/mult.vhdl
vcom $PATH/algorithms/div.vhdl
vcom $PATH/algorithms/or.vhdl
vcom $PATH/algorithms/beq.vhdl
vcom $PATH/algorithms/bgez.vhdl

# compile test benches
vcom $PATH/test/algorithms/tb_algo_*.vhdl

vsim tb_algo_beq
run -all
