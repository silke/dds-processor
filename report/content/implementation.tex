\section{Implementation}
The actual implementation is split in five components:
(i) a \emph{memory controller} which abstracts all memory access,
(ii) a \emph{decoder} which decodes instructions and sets the correct signals,
(iii) a \emph{register} which manages the registers,
(iv) a \emph{multi muxer} which contains all asynchronous muxing signals and the logic for branching,
and (v) the \emph{alu} (containing an \emph{adder}) that does the actual calculations.

The stages as described in \emph{analysis} are contained as follows:

\begin{itemize}
  \item \emph{Instruction fetch} is split between the \emph{memory controller}, which performs the fetching of the instruction, and the \emph{muxer} which increments the program counter.
        The \emph{decoder} buffers the instruction and program counter.
  \item \emph{Instruction decode} is implemented by the \emph{decoder}.
        \emph{Register fetch} is done by the \emph{register} with signals from the \emph{decoder}.
        The sign-extension is performed in the \emph{muxer}.
        The immediate value is to the \emph{muxer} by the \emph{decoder}
  \item \emph{Execute} is performed by the \emph{ALU}.
        The branch conditions are checked in the \emph{muxer}.
        The \emph{muxer} also sets the inputs of the ALU to the correct value.
  \item \emph{Memory access} is mostly performed by the \emph{memory controller} (because of the shared memory).
        The \emph{muxer} correctly sets the program counter and corresponding memory address.
  \item \emph{Write back} is performed by the \emph{muxer} which muxes the data between the \emph{memory controller} and the \emph{register}.
\end{itemize}

Some general design desisions have been made which affect the operation between stages:

\begin{itemize}
  \item Because a 32-bit memory address should always be aligned on a 4-byte boundary,
        the lower two bits of an address are always zero.
        These two bits are removed when passing the program counter between the decoder and the muxer,
        making the program counter a 30-bit integer.
        The muxer extends the last two bytes when required.
        This also ensures that the memory never receives an unaligned address.
\end{itemize}

\subsection{Memory controller}
\filen{implementation/memory_controller.vhdl}

\begin{figure}[ht]
  \centering
  \vspace{2em}
  \includegraphics{graph_memory_controller}
  \caption{State chart for the memory controller}%
  \label{fig:state:mc}
\end{figure}

The memory controller consists of a state machine that correctly communicates with the memory.
The interaction with the memory is as described in the behavioural description.
The protocol to interact with the memory controller is straight forward:

\begin{itemize}
  \item Writing:
        Put the data and address on the bus.
        Enable the write flag for one clock cycle.
        Keep the data and address on the bus until ready.
  \item Reading:
        Put the address on the bus.
        Enable the read flag for one clock cycle.
        Keep the address on the bus until ready.
        Read the data when ready.
\end{itemize}

Figure~\ref{fig:state:mc} shows the state chart for the memory controller.
The memory manager ensures that other parts that require memory access do not have to implement the complicated memory handshake protocol.
It is possible that the memory controller can skip the state where it waits for the memory to be ready if the memory is already idle.
The extra wait state can not be eliminated because the read and write input signals are only applied for a single clock tick.
While waiting for the memory to be ready, these signals could be deasserted.

\subsection{Decoder}
\filen{implementation/decoder.vhdl}

The decoder manages all parts to manage the instruction execution correctly.
This means that the decoder receives the instruction from the memory manager and uses the instruction to enable the multiplexers.
The decoder exists out of two parts:
(i) a state machine to keep track of the separate instruction stages of a single instruction and
(ii) multiple decoders to enable multiplexers in other parts of the design.

The state machine of the decoder is implemented as a Mealy machine.
The motivation of this design is that some inputs of the decoder can be applied directly as a different output, given that the state is correct.
For example, with the \texttt{lw} and \texttt{sw} instructions, the \lstinline{alu_rdy} signal is applied directly to the memory controller to signal the start of the memory controller.
An additional clock tick before the output of the decoder signals the memory controller start is not needed.

The decoder also stores the program counter.
Although all program counter calculations are done in the muxer and the ALU, the decoder stores the value and updates it at the end of an instruction cycle.

The inputs of the decoder are:

\begin{itemize}
  \item The data input from the memory controller, to read the next instruction.
  \item The memory controller ready signal.
  \item The next program counter as selected by the muxer.
  \item The ready signal of the ALU.
\end{itemize}
The ready signals are used to signal that the attached components have the new output data ready and that the decoder can advance to the next stage.

\begin{itemize}
  \item Read and write signals to the memory controller, to signal read and write operations.
  \item The program counter output. The muxer needs this to calculate the next instruction.
  \item Register addresses to the register map, directly mapped from the loaded instruction.
  \item Register write signal, to only enable a register write action when the ALU is ready and the instruction requires a store action to a register.
        For example, the multiply operation does not store a value to the register map.
  \item The immediate value from the instruction.
  \item Operation and enable signal to the ALU, to apply an operation and signal the start of a new operation to the ALU.
  \item Multiplexer controls, depending on the instruction, different data sources are needed. These controls select the multiplexer state.
\end{itemize}

Figure~\ref{fig:state:decoder} shows the state chart for the decoder.
Two extra states are needed for the \texttt{lw} and \texttt{sw} operation because they need an additional memory operation before completed.

\begin{figure}[ht]
  \centering
  \vspace{2em}
  \includegraphics{graph_decoder}
  \caption{State chart for the decoder}%
  \label{fig:state:decoder}
\end{figure}

\subsection{Register}
\filen{implementation/register.vhdl}

The register map has as a goal to provide a number of registers for reading and writing the data of the ALU.
Each register should be uniquely addressable and must be in a single clock operation readable and writable.
The register map needs to have two read paths and a single write path.

The register has as inputs:

\begin{itemize}
  \item The register addresses from the decoder, two addresses for the read data and a address to write to.
  \item An extra input is used to signal a register write. The register map only writes the input when this input is high.
  \item The data input from the Muxer.
\end{itemize}

The outputs of the register map are:

\begin{itemize}
  \item Two 32 bit data outputs for the selected registers.
\end{itemize}

Functionally, the register is not much more than multiplexers to read and write the correct addresses.
Both in- and outputs are done on the rising edge of the clock to ensure that the results are buffered.
Without the extra output buffering, the decoder control path would form via the register to the ALU a logic path without any buffering.
This logic path would require a slow clock to have enough time to stabilise the output signals of the ALU.

\subsection{Muxer}
\filen{implementation/muxer.vhdl}

The muxer's main goal is to connect the right signals to the right outputs.
As inputs it has:

\begin{itemize}
  \item Several flags for muxing the correct outputs.
        These flags determine the outputs and each instruction has a matching set of flags.
  \item The immediate value from the decoder.
  \item The program counter from the decoder.
  \item The data from the memory controller.
  \item The data from the ALU.
  \item The data from two registers.
\end{itemize}

The following outputs are set:

\begin{itemize}
  \item Next program counter.
        This is the result from the type of branch (equal, greater-equal-zero or none),
        and the result of the corresponding branch condition.
        The result is incremented.
  \item Memory address.
        This is either the program counter or the upper 30 bits of the data from the ALU (for instructions with memory access).
        The lower two bits are always zero.
  \item Register write data.
        This is either the result of the ALU or the data from the main memory.
  \item ALU input data.
        The first input is either the program counter (extended with two zeroes) or data from the first register.
        The second input is either the sign-extended immediate value or data from the second register.
\end{itemize}

\subsection{ALU}
\filen{implementation/alu.vhdl}

The arithmetic logic unit (ALU) is the component that actually runs the instructions.
It consists of a synchronous and an asynchronous part.

In the asynchronous part some calculations / muxes are always set:

\begin{itemize}
  \item The OR of the two inputs.
  \item Shifting the second input (which can be the immediate value) 16 bits to the left for the upper immediate.
  \item Calculating the sign of the two inputs for multiplication and division.
  \item The output data, mode and inputs of the adder ares set on the basis of the instruction and the state of the multiply/divide counter.
  \item Reset.
\end{itemize}

In the synchronous part the state (working or idle) is set or switched.
Almost all calculations are performed in a single clock cycle, which results in the ready flag being set after a single clock cycle.
The calculations that take longer (multiplication and division) keep track of their state with a counter, which triggers the ready flag after a set amount of operations.

\subsubsection{Multiplication}
The multiplier uses the multiplication algorithm as explained in section~\ref{sub:algomult}.
The operation has been divided into two stages: initialisation and execution.

Initialisation takes a single clock cycle in which the HILO register is initialised.
The HILO register has aliased for the HI and LO parts.

In the execution stage the shift and conditional addition is performed.
The addition is performed asynchronously in the adder, which has HI and what corresponds to \texttt{rt} as inputs. This takes 32 clock cycles.

\subsubsection{Division}
The divider uses the division algorithm as explained in section~\ref{sub:algodiv}.
The operation is divided into five stages: two preparation, execution and two post-operations.

The first initialisation step initialises HILO with 0 and the absolute value of \texttt{rs}, which is calculated by the adder (mode abs, inputs 0 and \texttt{rs}).
The second initialisation step stores the absolute value of \texttt{rt} from the adder (mode abs, inputs 0 and \texttt{rt}).

In the execution stages difference (again, calculated by the adder in sub mode, with inputs HILO[62..31] and the absolute of \texttt{rt}) and the shift/append operations are performed.

The first post-operation stores the negated value of LO (from the adder with inputs 0 and LO) if the calculated sign is 1. This corrects the sign of the dividend.
The second post-operation stores the negated value of HI (from the adder with inputs 0 and HI) if the sign of \texttt{rt} is 1. This corrects the sign of the remainder.

\subsubsection{Adder}
\filen{implementation/adder.vhdl}

The adder is designed in such a way that it can perform addition, subtraction, branch calculation, and can calculate the absolute value.
The executed operation is:

\[ r_d = s + (r_t \oplus r_x) + c \]

Where $r_d$ is the result, $s$ is the first parameter, $r_t$ is the (modified) second parameter, $r_x$ is either all ones or all zeroes (for inversion) and $c$ is a carry bit.

Table~\ref{tab:addvalues} shows the values of the parameters per operation, where $t$ is the original second parameter and $d$ the returned result.

\begin{table}[ht]
  \centering
  \caption{Value of parameters for each operation of the adder}%
  \label{tab:addvalues}
  \begin{tabular}{l r r r r}
    \toprule
    Operation & $r_t$ & $r_x(31..0)$ & $c$ & $d$ \\
    \midrule
    add             & $t$       & 0 & 0 & $rd$ \\
    sub             & $t$       & 1 & 1 & $rd$ \\
    slt             & $t$       & 1 & 1 & $rd(31)$ \\
    branch          & $t \ll 2$ & 0 & 0 & $rd$ \\
    abs $r_t \ge 0$ & $t$       & 1 & 0 & $rd$ \\
    abs $r_t   < 0$ & $t$       & 0 & 1 & $rd$ \\
    \bottomrule
  \end{tabular}
\end{table}

Note that it is still possible for the design to be simplified by performing the actions for the branch and absolute operations in the ALU,
turning them into add and add|sub respectively.
