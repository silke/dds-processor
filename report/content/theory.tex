\def\la{$\leftarrow$}
\section{Analysis}
The assignment is to implement a subset of the MIPS instruction set architecture (ISA).

\subsection{Requirements}
The assignment states:

\begin{quote}
  \begin{enumerate}
    \item Your processor should be optimized w.r.t. the area for the given instructions (area is more important than timing; although too slow is also not good).
    \item Your processor is only connected to a memory (no other devices such as a keyboard). You have to run a program that is stored in memory.
    \item It must be a synchronous system with a single clock (and no gated clocks).
    \item All design steps must be written in VHDL, and you are not allowed to use predefined IP-cores (it should be a technology independent description).
    \item We keep it simple, e.g.:
    \begin{enumerate}
      \item Internally use a 32 bits data bus.
      \item Use an unidirectional data bus for I/O.
      \item Do not use “bit-level” adder algorithms (e.g. ripple-adder, carry-look-ahead adder),
            but use behavioral description for addition (e.g. something like a+b).
      \item No interrupts.
      \item No exceptions.
      \item No pipelining.
    \end{enumerate}
  \end{enumerate}
\end{quote}

The requirement for no pipelining results in the processor not actually being a \emph{Microprocessor without Interlocked Pipeline Stages} (MIPS).

\subsection{Instruction definition}
The \citetitle{mips}\cite{mips} defines the bitwise layout of the instructions.
The instructions that need to be implemented can be split into R-type (with opcode 000000) and I-type instructions.

The first 16 byte of and instruction contain the \texttt{opcode},
source register (\texttt{rs}) and target (source/destination) register (\texttt{rt}).

\subsubsection{R-type instructions}
Register-type (R-type) instructions perform access to and from up to three registers.
The last 16 bits of the instruction contain the destination register (\texttt{rd}), the shift amount (\texttt{sa}, not used) and the function, which specifies the instruction.
The definitions for the R-type instructions that have to be implemented are shown in table~\ref{tab:r-type}.

\begin{table}[ht]
  \centering
  \caption{R-type instruction definition.}%
  \label{tab:r-type}
  \begin{bytefield}[bitwidth=9pt]{32}
      \bitheader{0,5,6,10,11,15,16,20,21,25,26,31} \\
      \bitbox{6}{opcode} &
          \bitbox{5}{rs} &
          \bitbox{5}{rt} &
          \bitbox{5}{rd} &
          \bitbox{5}{sa} &
          \bitbox{6}{function} \\
  \end{bytefield}

  \begin{tabular}{l l l l}
      \toprule
      Instruction & & Function & Description \\
      \midrule
      add  & rd, rs, rt & 100000 & rd \la\ rs + rt \\
      or   & rd, rs, rt & 100101 & rd \la\ rs | rt \\
      sub  & rd, rs, rt & 100010 & rd \la\ rs - rt \\
      slt  & rd, rs, rt & 101010 & rd \la\ (rs < rt) \\
      \midrule
      div  & rs, rt     & 011010 & LO \la\ rs / rt, HI \la\ rs \% rt \\
      mult & rs, rt     & 011000 & HI \& LO \la\ rs $\times$ rt \\
      \midrule
      mfhi & rd         & 010000 & rd \la\ HI \\
      mflo & rd         & 010010 & rd \la\ LO \\
      \midrule
      nop  & & 000000 & HALT \\
      \bottomrule
  \end{tabular}
\end{table}

\subsubsection{I-type}
Instruction type (I-type) instructions perform operations with two registers and an immediate value.
The last 16 bits of the instruction contain the immediate value (\texttt{imm}).
The definitions for the I-type instructions that have to be implemented are shown in table~\ref{tab:i-type}.


\begin{table}[ht]
  \centering
  \caption{I-type instruction definition.}%
  \label{tab:i-type}
  \begin{bytefield}[bitwidth=9pt]{32}
      \bitheader{0,15,16,20,21,25,26,31} \\
      \bitbox{6}{opcode} &
          \bitbox{5}{rs} &
          \bitbox{5}{rt} &
          \bitbox{16}{imm} \\
  \end{bytefield}

  \begin{tabular}{l l l l }
      \toprule
      Instruction & & Opcode & Description \\
      \midrule
      addi & rt, rs, imm & 001000 & rt \la\ rs + imm \\
      ori  & rt, rs, imm & 001101 & rt \la\ rs | imm \\
      lui  & rt, imm     & 001111 & rt \la\ imm << 16 \\
      \midrule
      lw   & rt, imm(rs) & 100011 & rt \la\ MEM[rs + imm] \\
      sw   & rt, imm(rs) & 101011 & MEM[rs + imm] \la\ rt \\
      \midrule
      beq  & rs, rt, imm & 000100 & rs = rt ? branch \\
      bgez & rs, imm     & 000001 & rs $\geq$ 0 ? branch \\
      \bottomrule
  \end{tabular}
\end{table}

In the complete ISA the \emph{bgez} instruction has the \emph{rt} field defined as 00001 to distinguish it from some other branch instructions.
As those instructions are not supported on the subset the MIPS architecture the value of this field should not matter.
All branch operations branch to the program counter of the instruction after the branch instruction incremented by (\texttt{imm << 2}).

\subsection{Instruction pipeline}
The standard MIPS architecture pipeline consists of several stages (figure~\ref{fig:eca_mips}):

\begin{itemize}
  \item Instruction fetch
  \item Instruction decode / register fetch
  \item Execute address calculation
  \item Memory access
  \item Write back
\end{itemize}

In the standard MIPS design, register between the stages ensure that the stages op the pipeline can be executed separately from each other.
While the target design is not pipelined, these steps will still need to be performed in the execution of an instruction.


\begin{figure}[ht]
  \centering
  \includegraphics{mips}
  \caption{Instruction pipeline for a MIPS processor (Source: slides for \emph{Embedded Computer Architecture 1})}\label{fig:eca_mips}
\end{figure}
