\section{Algorithms}
The algorithmic description of an instruction is a behavioural design that matches the targeted implementation as close as possible.
Creating the algorithmic descriptions allows them to be tested against the behavioural description separately.

For every instruction except \texttt{mfhi}, \texttt{mflo}, \texttt{lw}, \texttt{sw} and \text{nop} an algorithmic implementation is required, and the \texttt{add}, \texttt{addi}, \texttt{or} and \texttt{ori} instructions are defined using their behavioural description.

For every operation a package is defined. In this package functions for every devised algorithm and the behavioural description are implemented.

\subsection{Functional implementations}

\subsubsection{Subtraction}
\filen{algorithms/sub.vhdl}

It is possible to implement addition and subtraction using a single adder, as can be seen in figure~\ref{fig:add_subtract}.
Because the implementation of the adder is behavioural, the \texttt{sub} is implemented by using the \texttt{add} with a negated second argument:

% Subtraction implementation
\vlisting{43}{45}{algorithms/sub.vhdl}

Note that the second addition corresponds to the carry bit in figure~\ref{fig:add_subtract}.

\begin{figure}
  \centering
  \includegraphics{add_subtract}
  \caption{Subtraction using an adder (Source: slides for \emph{Computer Organisation})}%
  \label{fig:add_subtract}
\end{figure}

\subsubsection{Store less than}
\filen{algorithms/slt.vhdl}

The less-than algorithm is implemented by taking the difference between the numbers (ergo $s - t$) and returning the sign:

% SLT content
\vlisting{30}{37}{algorithms/slt.vhdl}

\subsubsection{Division}
\label{sub:algodiv}
\filen{algorithms/div.vhdl}

The chosen division algorithm (algo2)  is executed using \emph{restoring division}, which is performed in three phases:

\begin{enumerate}
  \item Take the absolute of the inputs, and initialise (\texttt{intermediate = concat(0,abs(s))},
    where intermediate consists of \texttt{concat(lo,hi)}):
  \item Execute for every bit (pseudocode):
    \begin{lstlisting}[autodedent,captionpos=]
      diff = (intermediate << 1) - abs(t)
      if diff < 0 then
        intermediate = intermediate << 1
      else
        intermediate = concat(diff, intermediate(31..0), 1);
      end
    \end{lstlisting}
  \item Take twos' complement of \texttt{lo} if one of the numbers is negative.
  \item Take twos' complement of \texttt{hi} if $s < 0$.
\end{enumerate}

This is a slow algorithm, but can be implemented using a single adder (see figure~\ref{fig:add_subtract}).
Because the implementation should be optimised for size, using a single adder is preferable.

Two additional division algorithms were tested.
For both algorithms, the aim was to implement a division algorithm that did not require compensation.
The first is the non-restoring algorithm, the other tries to do restoring division in ones' complement.
By doing the division in ones' complement, only simple inverters are needed for compensation, but no adder operation is required.
Unfortunatly both algorithm implementations failed in the sense that more compensation logic was needed than the logic saved by the lack of the restoring division compensation logic.

\subsubsection{Multiplication}
\label{sub:algomult}
\filen{algorithms/mult.vhdl}

The chosen multiplication algorithm is executed using a \emph{shift-add} algorithm:

\begin{enumerate}
  \item Initialise (\texttt{intermediate = concat(0, s)},
    where intermediate consists of \texttt{concat(lo,hi)}):
  \item Execute for every bit (pseudocode):
    \begin{lstlisting}[autodedent,captionpos=]
      if intermediate(0) == 1 then
        hi += t
      end
      intermediate >> 1  -- extended with sign of result
    \end{lstlisting}
\end{enumerate}

This is also a slow algorithm, and can also be implemented using a single adder.
Because the implementation should be optimised for size, using a single adder is preferable.
Although Booth's algorithm and modified Booth's algorithm could be used to speed up the multiplication, multiple shift distances are required to see an increase in performance.
With a single cycle shift-add or shift, implementing booth algorithm does not result in increased performance.

\subsubsection{Load upper immediate}
\filen{algorithms/lui.vhdl}

The result of the load-upper immediate is simply shifting the immediate 16 bits to the left, which corresponds to adding 16 zeroes at the end:

% Shift of lui
\vlisting{23}{23}{algorithms/lui.vhdl}

\subsubsection{Branch instructions}
\filen{algorithms/\{beq,bgez\}.vhdl}

The comparison of the \texttt{beq} instruction is done via a simple equality comparison.
\texttt{bgez} is compared via the sign bit.

The program counter is subsequently increased by four times the immediate value (ergo: left-shifted by 2, or extended by 2 zero bits).

\subsection{Algorithm testing}
\dirn{test/algorithms}

For every instruction a test bench to compare the behavioural description against the designed algorithm.
These tests run by \emph{fuzzing} (i.e. generating random values for) the input variables of the operation.
Fuzzing the inputs allows for relatively exhaustive testing without analysing all possible cases (which is error prone) or testing all possible values (which takes an eternity). Because the checks are purely functional, timing issues are not tested.

Below the test for the \texttt{sub} operation is shown to illustrate the basic working of such a test.

% loop in tb_algo_sub
\vlisting{44}{57}{test/algorithms/tb_algo_sub.vhdl}

Finally, the behavioural design with all instructions replaced by the chosen algorithm is tested in the same way as the behavioural design was tested.
