\section{Methodology}
\label{sec:methodology}

\subsection{Strategy}
The design for the processor is realised in several steps:

\begin{enumerate}
  \item Behavioural description of the processor
  \item Algorithm design
  \item Algorithm implementation
  \item Data path and control design
  \item Integration and synthesis
\end{enumerate}

All designs realised in the current step need to be tested thoroughly before starting the next step.
In every step all tests from the previous steps also need to remain functional.

All VHDL and auxiliary code should be under version control,
allowing the tracking of progress and reverting to older versions when bugs are introduced.

\begin{figure}[ht]
  \centering
  \includegraphics{git-merge}
  \caption{Example of a merge request with tests in progress}%
  \label{fig:git-merge}
\end{figure}

\begin{figure}[ht]
  \centering
  \includegraphics{git-merge-feedback}
  \caption{Example of feedback on a merge request}%
  \label{fig:git-merge-feedback}
\end{figure}

\subsection{Technology}
The tests are build using \emph{VUnit}, \emph{GHDL} and \emph{OSVVM}.
These are three cross-platform, free and open-source frameworks which in conjunction allow for automated testing.

The Git code repository is located on a GitLab instance provided by SNT\footnote{\url{https://git.snt.utwente.nl/dds/processor/}}.
Git allows work in parallel (on separate \emph{branches}) without the risk of overwriting another person's work.
By submitting a \emph{merge request} when a small part of the design is finished,
the other developers can review generated code.

These tools are used in conjunction with \emph{GitLab CI} for \emph{Continuous Testing}:
every time that code is \emph{pushed} to the Git code repository all tests run automatically.
This allows developers to have feedback on the submitted code,
while the tests ensure that changes in design never result in undesired behaviour.

An example of a \emph{merge request} showing a test in progress is shown in figure~\ref{fig:git-merge}. Feedback given on the merge request is shown in figure~\ref{fig:git-merge-feedback}.

\subsubsection{VUnit}
VUnit is\cite{vunit}:

\begin{quote}
[...] an open source unit testing framework for VHDL/\-SystemVerilog released under the terms of Mozilla Public License, v. 2.0. It features the functionality needed to realize continuous and automated testing of your HDL code. VUnit doesn’t replace but rather complements traditional testing methodologies by supporting a “test early and often” approach through automation.
\end{quote}

VUnit integrates with either ModelSim or GHDL for running tests and simulations and provides the OSVVM library for easing tests. It allows easy resolving of dependencies.
VUnit allows running a single test, a subset of all tests or all tests.
This allows components to have multiple tests that can be run as a group.

\subsubsection{GHDL}

GHDL is\cite{ghdl}:

\begin{quote}
  [...] an open-source simulator for the VHDL language. GHDL allows you to compile and execute your VHDL code directly in your PC.

  GHDL fully supports the 1987, 1993, 2002 versions of the IEEE 1076 VHDL standard, and partially the latest 2008 revision (well enough to support fixed\_generic\_pkg or float\_generic\_pkg).
\end{quote}

Using GHDL, a VHDL test or simulation can be performed on almost any system and in an automated fashion.
GHDL can also output the signals to a \emph{value change dump} (VCD) file, the format for which was defined along with the Verilog hardware description language.
This file can be read in almost any wave viewer.

GHDL compiles a test to native code, making it faster in simulations than ModelSim.
There is also (some) support for PSL.

This project resulted in one contribution to the GHDL code base.

\subsubsection{OSVVM}
Open Source VHDL Verification Methodology (OSVVM) provides\cite{osvvm}:

\begin{quote}
  [...] a methodology and library to simplify the entire verification effort. OSVVM supports the same capabilities that other verification languages support – from transaction level modeling, to functional coverage and randomized test generation, to data structures, and to basic utilities. The intention of OSVVM goes beyond capability though – OSVVM intends to make verification environments easy, readable, and fun.
\end{quote}

One of the major features of OSVVM is that it supports the generation of random values.
This allows \emph{fuzz testing} (running the program with random inputs) functions, procedures and components.

OSVVM also allows the generation of code coverage (the amount of code tested by a test),
which can be used to check if the created tests actually cover all situations.
