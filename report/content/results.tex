\section{Results}
\begin{figure*}[ht]
  \centering
  \includegraphics{wave_synth_timing}
  \margincaption{Waveform for the end of the post synthesis simulation%
  \label{fig:synth-timing}}
\end{figure*}

\subsection{Tests}
The final amount of tests is 22.
This include all tests starting with the behavioural testbed, and includes tests for all algorithms and datapath control implementations.
As can be seen below, all the tests pass.

\begin{lstlisting}[language=bash,morekeywords={pass,All, passed,!},keywordstyle=\color{Green}]
==== Summary =====================================
pass lib.tb_algo_beq.all       (2.5 seconds)
pass lib.tb_algo_bgez.all      (2.3 seconds)
pass lib.tb_behaviour.all      (14.1 seconds)
pass lib.tb_implementation.all (14.4 seconds)
pass lib.tb_algo.all           (16.2 seconds)
pass lib.tb_algo_lui.all       (3.4 seconds)
pass lib.tb_algo_slt.all       (2.1 seconds)
pass lib.tb_algo_or.all        (4.9 seconds)
pass lib.tb_algo_sub.all       (6.0 seconds)
pass lib.tb_alu_add.all        (8.6 seconds)
pass lib.tb_algo_div.all       (30.0 seconds)
pass lib.tb_alu_lui.all        (7.9 seconds)
pass lib.tb_alu_branch.all     (23.1 seconds)
pass lib.tb_alu_or.all         (7.6 seconds)
pass lib.tb_algo_mult.all      (44.3 seconds)
pass lib.tb_alu_slt.all        (5.8 seconds)
pass lib.tb_impl_reg.all       (1.8 seconds)
pass lib.tb_alu_sub.all        (6.6 seconds)
pass lib.tb_impl_memory.all    (1.6 seconds)
pass lib.tb_muxer.all          (6.1 seconds)
pass lib.tb_alu_div.all        (54.6 seconds)
pass lib.tb_alu_mult.all       (49.4 seconds)
==================================================
pass 22 of 22
==================================================
Total time was 313.4 seconds
Elapsed time was 92.1 seconds
==================================================
All passed!
\end{lstlisting}

\subsection{Simulation}

\begin{margintable}
  \centering
  \caption{Timing of operations in the implemented processor}
  \label{tab:opcycles}
  \begin{tabular}{l r}
    \toprule
    Operation & Cycles \\
    \midrule
    div   & 40 \\
    mult  & 37 \\
    lw    & 6  \\
    other & 5  \\
    \bottomrule
  \end{tabular}
\end{margintable}

The final design is simulated against the initial behavioural design.
By having the testbed include both the behavioural and the final design, differences between the two designs can be detected.
For the testbed we only checked if the result of every operation was identical.
The result of every memory operation of the implementation was tested against the behavioural design.
The synthesizeable design takes multiple clock cycles for an operation where the behavioural operates on single clock cycles.
This difference is solved by detaching the read and write request lines to the memory from the behavioural design.
The memory only receives the read and write signals from the implementation.
It appears to the behavioural design like the memory sometimes has a slow response time, but the behavioural design functions under these constraints.
The memory read addresses from both designs can be compared when the implementation issues an operation.

\begin{margintable}[-30em]
  \centering
  \caption{Distribution of time spent processing and waiting for memory}
  \label{tab:proctimes}
  \begin{tabular}{l r r}
    \toprule
    Task & Cycles & \% \\
    \midrule
    total & 5515 & 100 \\
    processor & 4573 & 83 \\
    memory & 942 & 17 \\
    \bottomrule
  \end{tabular}
\end{margintable}

\begin{margintable}[-8em]
  \centering
  \caption{Time taken by operations and share of processing time}
  \label{tab:optimes}
  \begin{tabular}{l r r r}
    \toprule
    Op & Count & Cycl. & \% \\
    \midrule
    total & 565 & 4573 & 100,0 \\
    mult & 48 & 1776 & 38,8 \\
    add & 87 & 435 & 9,5 \\
    addi & 81 & 405 & 8,9 \\
    bgez & 73 & 365 & 8,0 \\
    slt & 61 & 305 & 6,7 \\
    beq & 61 & 305 & 6,7 \\
    mflo & 54 & 270 & 5,9 \\
    div & 6 & 240 & 5,2 \\
    sub & 42 & 210 & 4,6 \\
    or & 32 & 160 & 3,5 \\
    ori & 14 & 70 & 1,5 \\
    lui & 3 & 15 & 0,3 \\
    lw & 2 & 12 & 0,3 \\
    sw & 1 & 5 & 0,1 \\
    mfhi & 0 & 0 & 0,0 \\
    \bottomrule
  \end{tabular}
\end{margintable}

With the final simulation, the resulting instruction timings can be analysed.
The processor takes 5515 clock cycles (equivalent to \SI{77.21}{\micro\second} at \SI{71,43}{\mega\hertz}) to execute the entire program.
From the number of cycles per operation (table~\ref{tab:opcycles}) the distribution of the processing time can be calculated.
The processor spends 17 percent of the time waiting for the memory (table~\ref{tab:proctimes}).
When processing, it spends 39 percent of the time on the multiply operation (table~\ref{tab:optimes}).

\subsection{Synthesis}

\begin{margintable}
  \centering
  \caption{Power estimation for the synthesized design from Quartus.}%
  \label{tab:powerest}
  \begin{tabular}{l R{7em}}
    \toprule
    Consumer & Thermal power dissipation \\
    \midrule
    Total & 157.33 mW \\
    Core dynamic & 37.24 mW \\
    Core static & 47.51 mW \\
    I/O & 72.58 mW \\
    \bottomrule
  \end{tabular}
\end{margintable}

Synthesis of the design is performed with Quartus.
The synthesis process generates the necessary files to perform post simulation and analysis.
From the synthesis generated by quartus, the design of the processor can be confirmed with the RTL view (figure~\ref{fig:synth-rtl}).
Here, the indiviual processor components as their interconnects are clearly visualised.

Post simulation was performed with the files generated by Quartus. From the simulation waveforms, it is visible that the output signals stabilise when the clock is already low (figure~\ref{fig:synth-timing}).
While this performs within the requirements, the design becomes unstable with a higher clock speed.
Quartus agrees on this, estimating a maximum clock speed of \SI{74.87}{\mega\hertz} with the slow TimeQuest model.

The design takes around 15 percent of the logic element space on a Cyclone II EP2C20F484C7.

\begin{table}[ht]
  \centering
  \caption{Number of elements used in the synthesized design on a Cyclone II EP2C20F484C7}%
  \label{tab:synt-elements}
  \begin{tabular}{l r r}
    \toprule
    Type of element & Amount & \% \\
    \midrule
    Total available            &  18,752 & 100 \\
    Total logic                &  2,720  & 15 \\
    Total combinational        &  2,273  & 12 \\
    Dedicated logic registers  &  1,297  &  7 \\
    \bottomrule
  \end{tabular}
\end{table}

\subsubsection{Power estimation}
Table~\ref{tab:powerest} shows the power estimation from Quartus.
The `Power Estimation Confidence' is given as \emph{Low}\footnote{Low: user provided insufficient toggle rate data}.
In the design the I/O thermal power dissipation is dominant.
This can be attributed to the large amount of communication with the memory.
The static power usage (leakage) can be attributed to the fact that a relatively small portion of the FPGA is used by the synthesized design.

\clearpage
\begin{figure*}[ht]
  \centering
  \margincaption{RTL view of the synthesized design from Quartus.%
  \label{fig:synth-rtl}}[-17cm]
  \hspace*{-5cm}\includegraphics[angle=90,origin=c,width=1\textwidth]{rtl_implementation}
\end{figure*}
